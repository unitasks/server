import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import {Button} from "react-bootstrap";
import axios from "axios";
import {toast} from "react-toastify";
import {getToken} from "../util/cookie_manager";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";
import ContentEditorLight from "../fragments/content_editor_light";
import {history} from "../index";
import {handleErrors} from "../util/error_manager";


class TaskEditor extends Component {

    constructor(props) {
        super(props);

        this.nameRef = React.createRef();
        this.descriptionRef = React.createRef();

        this.isPersonalSwitch = React.createRef();
        this.groupsSelect = React.createRef();

        this.groupsMap = new Map();

        let newTask;

        newTask = typeof this.props.task_id === "undefined";


        this.state = {
            isNewTask: newTask,
            taskId: this.props.task_id,
            initialized: newTask && !this.props.is_subtask,
            descriptionValue: null,
            titleValue: "",
            selectedDay: undefined,
            isDateEmpty: true,
            saveButtonEnabled: !newTask,
            select_groups_options: null
        };
        this.handleDayChange = this.handleDayChange.bind(this);
    }

    handleChange(event) {
        let saveButtonEnabled_;

        saveButtonEnabled_ = this.nameRef.current.value !== "";

        this.setState({
            titleValue: this.nameRef.current.value,
            descriptionValue: this.descriptionRef.current.getCurrentContent(),
            saveButtonEnabled: saveButtonEnabled_
        });
    }

    pressSaveButton() {
        if (this.state.isDateEmpty) {
            toast.error("Please enter a date");
        } else {
            let taskdata = {
                title: this.nameRef.current.value,
                description: JSON.stringify(this.descriptionRef.current.getCurrentContent()),
                datetime: this.state.selectedDay.toLocaleString("en-US"),
                is_personal: this.state.type === "personal",
                group_id: this.state.group_id
            };

            if (this.state.isNewTask) {
                if (!this.props.is_subtask) {
                    if (!this.isPersonalSwitch.current.checked) {
                        taskdata["is_personal"] = false;
                        taskdata["group_id"] = this.groupsMap.get(this.groupsSelect.current.value).groupid;
                    } else {
                        taskdata["is_personal"] = true;
                    }
                }

                taskdata.is_subtask = this.props.is_subtask;
                if (!this.props.is_subtask) {
                    taskdata.root = null;
                } else {
                    taskdata.root = this.props.parent_task;
                }

                axios.post("/api/task", taskdata,
                    {headers: {Authorization: "Bearer " + getToken()}})
                    .then((response) => {
                        let task_id = response.data.taskid;
                        window.location.href="/taskList";
                        toast.success("Task successfully created!");
                    }).catch((error) => {
                        handleErrors(error);
                });
            } else {
                axios.put("/api/task/" + this.state.taskId, taskdata,
                    {headers: {Authorization: "Bearer " + getToken()}})
                    .then((response) => {
                        window.location.href="/taskList";
                        toast.success("Task successfully updated!");
                    }).catch((error) => {
                        handleErrors(error);
                });
            }
        }
    }

    cancelEditing() {
        history.goBack();
    }

    handleDayChange(selectedDay, modifiers, dayPickerInput) {
        const input = dayPickerInput.getInput();
        this.setState({
            selectedDay: selectedDay,
            isDateEmpty: !input.value.trim(),
        });
    }

    loadExistingTask() {
        if (!this.state.isNewTask && !this.props.is_subtask) {
            axios.get("/api/task/" + this.state.taskId,
                {headers: {Authorization: "Bearer " + getToken()}})
                .then(
                    (response) => {
                        let group_id_ = null;
                        if (response.data.type === "group") {
                            group_id_ = response.data.groupid;
                        }

                        this.setState({
                            titleValue: response.data.title,
                            descriptionValue: JSON.parse(response.data.description),
                            selectedDay: new Date(response.data.duedate),
                            initialized: true,
                            isDateEmpty: false,
                            type: response.data.type,
                            group_id: group_id_
                        });
                    }
                );
        } else if (this.state.isNewTask && this.props.is_subtask) {
            axios.get("/api/task/" + this.props.parent_task,
                {headers: {Authorization: "Bearer " + getToken()}})
                .then(
                    (response) => {
                        let group_id_ = null;
                        if (response.data.type === "group") {
                            group_id_ = response.data.groupid;
                        }
                        this.setState({
                            selectedDay: new Date(response.data.duedate),
                            isDateEmpty: false,
                            initialized: true,
                            type: response.data.type,
                            group_id: group_id_
                        });
                    }
                );
        } else if (!this.state.isNewTask && this.props.is_subtask) {
            axios.get("/api/task/" + this.props.task_id,
                {headers: {Authorization: "Bearer " + getToken()}})
                .then(
                    (response) => {
                        this.setState({
                            titleValue: response.data.title,
                            descriptionValue: JSON.parse(response.data.description)
                        });

                        axios.get("/api/task/" + response.data.parent,
                            {headers: {Authorization: "Bearer " + getToken()}})
                            .then(
                                (response) => {
                                    let group_id_ = null;
                                    if (response.data.type === "group") {
                                        group_id_ = response.data.groupid;
                                    }

                                    this.setState({
                                        selectedDay: new Date(response.data.duedate),
                                        isDateEmpty: false,
                                        initialized: true,
                                        type: response.data.type,
                                        group_id: group_id_
                                    });
                                }
                            );
                    }
                )

        }
    }

    getTitle() {
        if (!this.props.is_subtask) {
            if (this.state.isNewTask) {
                return "Create a Task";
            } else {
                return "Update Task";
            }
        } else {
            if (this.state.isNewTask) {
                return "Create a Subtask";
            } else {
                return "Update Subtask";
            }
        }
    }

    getGroupOptions(groups) {
        let groupOptions = [];
        for (let i = 0; i < groups.length; i++) {
            groupOptions.push(<option>{groups[i].name}</option>);
        }
        return groupOptions;
    }

    getGroups() {
        if (this.state.select_groups_options === null) {
            axios.get("/api/groups", {headers: {Authorization: "Bearer " + getToken()}}).then(
                (response) => {
                    let groups = response.data;

                    let adminGroups = [];

                    for (let i = 0; i < groups.length; i++) {
                        if (groups[i].admin) {
                            adminGroups.push(groups[i]);
                            this.groupsMap.set(groups[i].name, groups[i]);
                        }
                    }

                    this.setState({
                        select_groups_options: adminGroups
                    });
                }
            ).catch(
                (error) => {
                   handleErrors(error);
                }
            );
            return <option>Loading...</option>;
        } else {
            return this.getGroupOptions(this.state.select_groups_options);
        }
    }

    getFullTaskForms() {
        if (!this.props.is_subtask) {
            return (<div><Form.Group controlId={"formDueDate"}>
                <DayPickerInput
                    value={this.state.selectedDay}
                    onDayChange={this.handleDayChange}
                />
            </Form.Group>

                <Form.Group controlId={"formGroup"}>
                    <Form.Check type={"switch"} ref={this.isPersonalSwitch} label={"Personal task"}/>
                    <Form.Control ref={this.groupsSelect} as="select">
                        {this.getGroups()}
                    </Form.Control>
                </Form.Group></div>);
        } else{ return <div/>;}
    }

    render() {
        if (this.state.initialized) {
            return (
                <div className={"container"}>
                    <h1>{this.getTitle()}</h1>
                    <Form className={"my-4"}>
                        <Form.Group controlId={"formName"}>
                            <Form.Control type={"text"} value={this.state.titleValue} ref={this.nameRef}
                                          placeholder={"Name"}
                                          onChange={(e) => this.handleChange(e)}/>
                        </Form.Group>

                        <Form.Group controlId={"formDescription"}>
                            <ContentEditorLight ref={this.descriptionRef} content={this.state.descriptionValue}/>
                        </Form.Group>

                        {this.getFullTaskForms()}

                        <Button disabled={!this.state.saveButtonEnabled} variant={"primary"}
                                className={"mr-2"} onClick={() => this.pressSaveButton()}>
                            {this.state.isNewTask ? "Create Task" : "Update Task"}
                        </Button>
                        <Button style={{float: "right"}} variant={"secondary"} className={"mr-2 float-right"}
                                onClick={() => this.cancelEditing()}>
                            Cancel
                        </Button>
                    </Form>

                </div>
            );
        } else {
            this.loadExistingTask();
            return <div>Loading...</div>;
        }

    }
}

export default TaskEditor;