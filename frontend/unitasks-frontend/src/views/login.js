import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import { Button } from "react-bootstrap";
import { checkEmail, checkPassword, checkUsername } from "../util/validator";
import { saveTokenCookie } from "../util/cookie_manager";
import { toast } from "react-toastify";
import { getInstance } from "../util/event_system";
import { handleErrors } from "../util/error_manager";
import { authorisation, history } from "../index";

class LoginPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            initialized: false,
            joinToken: undefined
        };

        this.validations = {
            "user": false,
            "password": false
        };

        this.userRef = React.createRef();
        this.passwordRef = React.createRef();
        this.submitRef = React.createRef();
        this.state = { submitEnabled: false };
    }

    checkJoinLink() {
        if (this.props.match.url.includes("loginWithJoinToken")) {
            let joinToken = this.props.match.params.join_token;
            if (joinToken !== undefined) {
                this.setState({ initialized: true, joinToken });
            } else {
                toast.error("No join link token found!");
            }
        } else {
            this.setState({ initialized: true });
        }
    }

    handleChange(event) {
        switch (event.target.id) {
            case "formUser":
                this.validations["user"] = checkEmail(this.userRef.current.value) || checkUsername(this.userRef.current.value);
                break;
            case "formPassword":
                this.validations["password"] = checkPassword(this.passwordRef.current.value);
                break;
        }

        this.setState({ submitEnabled: this.allValidated() });
    }

    allValidated() {
        let out = true;
        for (let key in this.validations) {
            if (!this.validations[key]) {
                out = false;
                break;
            }
        }

        return out;
    }

    login() {
        authorisation.login(this.userRef.current.value, this.passwordRef.current.value)
            .then((response) => {
                saveTokenCookie(response.data.access_token);
                toast.success("Login successful!");
                getInstance().triggerEvent("login");

                if (this.state.joinToken !== undefined) {
                    this.props.history.push("/group/join/" + this.state.joinToken);
                } else {
                    const { from } = this.props.location.state || { from: { pathname: "/" } };
                    history.push(from);
                }
            }).catch(()=> {
                toast.error("Username or password incorrect!");
            });
    }

    render() {
        if (!this.state.initialized) {
            this.checkJoinLink();
            return <div>Loading...</div>;
        } else {
            return (
                <div className={"container"}>
                    <h1>Sign In</h1>
                    <Form className={"my-4"}>
                        <Form.Group controlId={"formUser"}>
                            <Form.Control type={"text"} ref={this.userRef} placeholder={"Email / Username"}
                                onChange={((e) => this.handleChange(e))} />
                            <Form.Text className={"text-muted"}>
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId={"formPassword"}>
                            <Form.Control type={"password"} ref={this.passwordRef} placeholder={"Password"}
                                onChange={(e => this.handleChange(e))} />
                        </Form.Group>
                        <Form.Group controlId={"formRemember"}>
                            <Form.Check type={"checkbox"} label={"Remember me"} />
                        </Form.Group>

                        <Button onClick={this.login.bind(this)} disabled={!this.state.submitEnabled}
                            ref={this.submitRef}
                            variant={"primary"} className={"mr-2"}>
                            Submit
                        </Button>

                        <Button variant={"outline-secondary"}>
                            Forgot password
                        </Button>

                    </Form>
                </div>
            );
        }
    }
}

export default LoginPage;