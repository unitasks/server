import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import {Button} from "react-bootstrap";
import axios from "axios";
import {toast} from "react-toastify";
import {getToken} from "../util/cookie_manager";
import Alert from "react-bootstrap/Alert";
import Table from "react-bootstrap/Table";
import MaterialIcon from "material-icons-react";
import Modal from "react-awesome-modal";
import {history} from "../index";
import {handleErrors} from "../util/error_manager";

class Group extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedId: undefined,
            isNewGroup: false,
            isAdmin: false,
            displayMode:false,
            groupMembers:undefined,
            showDialog:false,
            saveButtonEnabled:false,
            showAddMemberDialog:false,
            addMemberDialogButtonEnabled:false,
            initialized:false,
            joinLink:"",
            groupName:"",
            groupDescription:"",
            noAdminErrorShown:false,
        };
        this.groupName = React.createRef();
        this.groupDescription = React.createRef();
        this.memberName = React.createRef();
    }

    loadData(){
        let id = this.props.match.params.id;
        if (id === undefined) {
            this.setState({isNewGroup: true, initialized: true});
        } else {
            axios.get("/api/group/" + id, {"headers": {"Authorization": "Bearer " + getToken()}})
                .then((res) => {
                    this.setState({
                        groupMembers: res.data.members,
                        joinLink: "staging.unitasks.net/group/join/" + res.data.join_link,
                        groupName: res.data.name,
                        groupDescription: res.data.description,
                        isAdmin: res.data.admin
                    });
                }).catch((error) => {
                handleErrors(error);
            }).finally(() => {
                if (this.state.isAdmin) {
                    if (this.props.match.url.includes("displayGroup")) {
                        this.setState({selectedId: id, isNewGroup: false, displayMode: true, initialized: true});
                    } else {
                        this.setState({selectedId: id, isNewGroup: false, displayMode: false, initialized: true});
                    }
                } else {
                    if (!this.state.noAdminErrorShown) {
                        this.setState({noAdminErrorShown: true});
                        toast.error("You are no admin of this group");
                        this.props.history.push("/group");
                    }
                }
            });
        }
    }

    saveGroup(){
         let data = {
            name : this.groupName.current.value,
            description : this.groupDescription.current.value
         };
        if(this.state.isNewGroup){
            axios.post("/api/group", data,{ headers: { Authorization: "Bearer " +getToken() }})
            .then((response) => {
                toast.success("Creation of group successful!");
                this.props.history.push("/displayGroup/"+response.data.groupId);
            }).catch((error) => {
                handleErrors(error);
            });
        }else{
            axios.put("/api/group/"+this.state.selectedId, data,{ headers: { Authorization: "Bearer " +getToken() }})
            .then((response) => {
                toast.success("Update of group successful!");
                this.props.history.push("/displayGroup/"+this.state.selectedId);
            }).catch((error) => {
                handleErrors(error);
            });
        }
    }

    deleteGroup(){
        this.setState({showDialog:false});
        axios.delete("/api/group/"+this.state.selectedId, { headers: { Authorization: "Bearer " +getToken() }})
            .then((res) => {
                toast.success("Group successfully deleted!");
            }).catch((error) => {
                handleErrors(error);
            });
        this.props.history.push("/group");
    }

    cancelEditing(){
        history.goBack();
    }


    renderTableData() {
      return this.state.groupMembers.map((member, index) => {
         const { userid, displayname, username, is_admin} = member;
         return (
            <tr key={userid}>
               <td>{displayname}</td>
               <td>{username}</td>
               <td><Form.Check type="checkbox" label={(is_admin?"Group Admin":"No Admin")} checked={is_admin} onChange={() => this.changeAdminRights(userid,is_admin)}/></td>
               <td>
                    <a onClick={() => this.removeMember(userid)} style={{ cursor: "pointer"}}>
                    <div>
                        <MaterialIcon icon={"person_remove"}/>
                    </div>
                   </a>
               </td>
            </tr>
         );
      });
   }

   removeMember(userId){
        axios.delete("/api/group/"+ this.state.selectedId +"/member/"+userId, { headers: { Authorization: "Bearer " +getToken() }})
            .then((res) => {
                toast.success("Member successfully removed!");
                this.setState({
                    initialized: false
                });
            }).catch((error) => {
                handleErrors(error);
            });
   }

   changeAdminRights(userId,wasGroupAdmin){
        axios.put("/api/group/"+this.state.selectedId+"/member/"+userId, { isAdmin : !wasGroupAdmin}, { headers: { Authorization: "Bearer " +getToken() }})
            .then((response) => {
                this.setState({
                    initialized: false
                });
            }).catch((error) => {
                 handleErrors(error);
            });
   }

   addMember(){
        this.closeAddMemberDialog();
        axios.post("/api/group/"+this.state.selectedId+"/member/"+this.memberName.current.value, this.memberName.current.value,{ headers: { Authorization: "Bearer " +getToken() }})
            .then((response) => {
                this.memberName.current.value="";
                this.setState({
                    initialized: false,
                    addMemberDialogButtonEnabled: false
                });
            }).catch((error) => {
                this.memberName.current.value="";
                handleErrors(error);
            });
   }

   closeAddMemberDialog(){
        this.setState({showAddMemberDialog:false});
   }

   copyJoinLink(){
       const el = document.createElement("textarea");
       el.value = this.state.joinLink;
       el.setAttribute("readonly", "");
       el.style.position = "absolute";
       el.style.left = "-9999px";
       document.body.appendChild(el);
       const selected =
        document.getSelection().rangeCount > 0        // Check if there is any content selected previously
          ? document.getSelection().getRangeAt(0)
          : false;
       el.select();
       document.execCommand("copy");
       document.body.removeChild(el);
       if (selected) {                                 // If a selection existed before copying
         document.getSelection().removeAllRanges();
         document.getSelection().addRange(selected);   // Restore the original selection
       }
       toast.success("Join link copied to clipboard!");
   }

   handleChange(event){
        switch (event.target.id ) {
            case "formDialog":
                if(this.memberName.current.value !== ""){
                    this.setState({addMemberDialogButtonEnabled:true});
                }else{
                    this.setState({addMemberDialogButtonEnabled:false});
                }
                break;
            default:
                if(this.groupName.current.value !== "" && this.groupDescription.current.value !== ""){
                    this.setState({
                        groupName: this.groupName.current.value,
                        groupDescription:this.groupDescription.current.value,
                        saveButtonEnabled:true
                    });
                }else{
                    this.setState({
                        groupName: this.groupName.current.value,
                        groupDescription:this.groupDescription.current.value,
                        saveButtonEnabled:false
                    });
                }

        }

   }


    render() {
         if(!this.state.initialized) {
            this.loadData();
            return <div>Loading...</div>;
        }else {
             let heading;
             let form = <Form className={"my-4"}>
                 <Form.Group controlId={"formGroupName"}>
                     <Form.Control type={"text"} value={this.state.groupName} ref={this.groupName} placeholder={"Group Name"}
                                   onChange={(e) => this.handleChange(e)}/>
                 </Form.Group>
                 <Form.Group controlId={"formDescription"}>
                     <Form.Control type={"text"} value={this.state.groupDescription} ref={this.groupDescription} placeholder={"Group Description"}
                                   onChange={(e) => this.handleChange(e)}/>
                 </Form.Group>
             </Form>;

             let buttons = <div>
                 <Button className={"mr-2"} onClick={(() => this.saveGroup())} variant={"primary"} disabled={!this.state.saveButtonEnabled}>Save Group</Button>
                 <Button variant={"outline-danger"} className={"mr-2"}
                         onClick={() => this.setState({showDialog: true})}>Delete Group</Button>
                 <Button style={{float: "right"}} variant={"secondary"} className={"mr-2 float-right"}
                         onClick={() => this.cancelEditing()}>Cancel</Button>
             </div>;

             let content = "";

             let displayModeButtons = <div>
                 <Button style={{float: "right"}} className={"mb-2 float-right"}
                         onClick={(() => this.setState({showAddMemberDialog: true}))} variant={"success"}>Add
                     Member</Button>
                 <Button style={{float: "right"}} variant={"secondary"} className={"mb-2 mr-2 float-right"}
                         onClick={() => this.copyJoinLink()}>Copy Join Link</Button>
             </div>;
             if (this.state.isNewGroup) {
                 heading = <h1>Create a Group</h1>;
                 displayModeButtons = "";
                 buttons = <div>
                     <Button className={"mr-2"} onClick={(() => this.saveGroup())} variant={"primary"} disabled={!this.state.saveButtonEnabled}>Save
                         Group </Button>
                     <Button style={{float: "right"}} variant={"secondary"} className={"mr-2 float-right"}
                             onClick={() => this.cancelEditing()}>Cancel</Button>
                 </div>;
             } else {
                 content=<Table striped bordered>
                             <thead>
                             <tr>
                                 <th>Displayname</th>
                                 <th>Username</th>
                                 <th>Group Admin</th>
                                 <th>Remove</th>
                             </tr>
                             </thead>
                             <tbody>
                             {this.renderTableData()}
                             </tbody>
                         </Table>;
                 if (!this.state.displayMode) {
                     heading = <h1>Edit Group</h1>;
                 } else {
                     form= "";
                     heading = <div><h1>{this.state.groupName}</h1><h4>{this.state.groupDescription}</h4></div>;
                     buttons = <Button className={"mt-2"}
                                       onClick={(() => this.props.history.push("/editGroup/" + this.state.selectedId))}
                                       variant={"primary"}>Edit Group</Button>;
                 }
             }
             return (
                 <div className={"container"}>
                     <Modal visible={this.state.showAddMemberDialog} width="400" height="200" effect="fadeInUp"
                            onClickAway={() => this.closeAddMemberDialog()}>
                         <div className={"container"} style={{padding: 20}}>
                             <h4>Add a member to this group!</h4>
                             <Form className={"my-4"}>
                                 <Form.Group controlId={"formDialog"}>
                                     <Form.Control type={"text"} ref={this.memberName} onChange={(e) => this.handleChange(e)} placeholder={"Username"}/>
                                 </Form.Group>
                                 <Button onClick={(() => this.addMember())} variant={"primary"} disabled={!this.state.addMemberDialogButtonEnabled}>Add Member </Button>
                                 <Button style={{float: "right"}} variant={"secondary"} className={"mr-2 float-right"}
                                         onClick={(() => this.closeAddMemberDialog())}>Cancel </Button>
                             </Form>
                         </div>
                     </Modal>
                     {heading}
                     <Alert show={this.state.showDialog} variant="danger">
                         <Alert.Heading>Delete group</Alert.Heading>
                         <p>
                             Are you sure you want to delete this group? This cannot be undone.
                         </p>
                         <div className="d-flex justify-content-end">
                             <Button onClick={(() => this.deleteGroup())} variant="outline-danger">
                                 Delete Group
                             </Button>
                             <Button onClick={() => this.setState({showDialog: false})} className={"ml-2"}
                                     variant="outline-secondary">
                                 Cancel
                             </Button>
                         </div>
                     </Alert>
                     {displayModeButtons}
                     {form}
                     {content}
                     {buttons}
                 </div>
             );
         }
    }
}

export default Group;