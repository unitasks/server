import React, {Component} from "react";
import ExchangeThread from "../fragments/exchange_message";
import {Button} from "react-bootstrap";
import axios from "axios";
import {getToken} from "../util/cookie_manager";
import {getUserData} from "../util/user_data";
import {handleErrors} from "../util/error_manager";


class ExchangeBoard extends Component {

    constructor(props) {
        super(props);
        this.all_threads = new Map();
        this.counter = 0;
        this.state = {
            rootThreads: []
        };
        this.lastUpdated = new Date(1700, 1, 1);
        this.task_id = typeof props.task_id === "undefined" ? 50 : props.task_id;
    }

    initializeThread(element, id) {
        this.all_threads.set(id, element);
    }

    changeIdInThreadRegistry(old_id, new_id) {
        let obj = this.all_threads.get(old_id);
        this.all_threads.set(new_id, obj);
        this.all_threads.delete(old_id);
    }

    craftExchangeMessage(is_root, owner, tid, initialMode, content, parent, has_children, editable) {
        let changing;
        let mode;
        let content_;

        switch (initialMode) {
            case "view":
                changing = false;
                mode = null;
                content_ = content;
                break;
            case "edit":
                changing = true;
                mode = "edit";
                content_ = content;
                break;
            case "create":
                changing = "true";
                mode = "create";
                content_ = null;
                break;
        }

        return <ExchangeThread exchange_board={this} owner={owner} message_id={tid}
                               ref={(el) => this.initializeThread(el, tid)}
                               editable={editable} is_root={is_root} mode={mode} changing={changing} content={content_}
                               parent={parent} has_children={has_children}
        />;
    }

    getDatetimeAsPythonCompatibleISOString() {
        let timestr = this.lastUpdated.toISOString();
        return timestr.substr(0, timestr.length - 1);
    }

    getParentFromResponseArray(response_array, parent_id) {
        for (let i = 0; i < response_array.length; i++) {
            let currentMsg = response_array[i];

            if (currentMsg.message_id === parent_id) {
                return currentMsg;
            }
        }
        return null;
    }

    createNewFromPolling(msg, response_array) {
        if (msg.parent_id !== null) {
            if (!this.all_threads.has(msg.parent_id)) {
                this.createNewFromPolling(this.getParentFromResponseArray(response_array, msg.parent_id), response_array);
            }

            let parent = this.all_threads.get(msg.parent_id);

            parent.addChild(this.craftExchangeMessage(false, msg.owner_name, msg.message_id, "view", msg.content, msg.parent_id, msg.has_children, msg.editable));

        } else {
            let root_threads = this.state.rootThreads.slice();
            {
                root_threads.push(this.craftExchangeMessage(true, msg.owner_name, msg.message_id, "view", msg.content, null,
                    msg.has_children, msg.editable));
                this.setState({rootThreads: root_threads});
            }
        }
    }

    pollForUpdates() {
        let messages_string = "";
        this.all_threads.forEach((value, key, map) => {
            messages_string += key + ",";
        });
        messages_string = messages_string.substr(0, messages_string.length - 1);

        axios.get("/api/task/" + this.task_id + "/message/update?last_updated=" + this.getDatetimeAsPythonCompatibleISOString() + "&messages=[" + messages_string + "]", {
            headers: {
                Authorization: "Bearer " + getToken()
            }
        }).then((response) => {
            let messages = response.data;

            for (let i = 0; i < messages.length; i++) {
                let msg = messages[i];
                if (this.all_threads.has(msg.message_id)) {
                    let msgOnPage = this.all_threads.get(msg.message_id);
                    // If message is being edited right now, do not apply updates!
                    if (!msgOnPage.state.changing) {
                        msgOnPage.owner = msg.owner_name;
                        msgOnPage.setState({
                            content: msg.content,
                            has_children: msg.has_children,
                        });
                    }
                } else {
                    this.createNewFromPolling(msg, messages);
                }
            }

            this.lastUpdated = new Date();
        }).catch((error) => {
            handleErrors(error);
        });
    }

    createChildMessage(parent, tid) {
        this.all_threads.get(parent).addChild(this.craftExchangeMessage(false, getUserData().displayName, tid, "create", null, parent, false, true));
    }

    injectChildMessage(parent, tid, content) {
        this.all_threads.get(parent).addChild(this.craftExchangeMessage(false, getUserData().displayName, tid, "view", content, parent, false, true));
    }

    getRootThreads() {
        let root_threads_elements = [];
        for (let i = 0; i < this.state.rootThreads.length; i++) {
            let root_thread = this.state.rootThreads[i];
            root_threads_elements.push(root_thread);
        }

        return root_threads_elements;
    }

    componentDidMount() {
        axios.get("/api/task/" + this.task_id + "/message", {
            headers: {
                Authorization: "Bearer " + getToken()
            }
        }).then((response) => {
                let messages = response.data.messages;
                let root_threads = this.state.rootThreads.slice();

                for (let msg_index = 0; msg_index < messages.length; msg_index++) {
                    let msg = messages[msg_index];
                    root_threads.push(this.craftExchangeMessage(true, msg.owner_name, msg.message_id, "view", msg.content, null,
                        msg.has_children, msg.editable));
                    //this.injectRootMessage(msg.message_id, msg.content, )
                }
                this.setState({rootThreads: root_threads});

                this.pollingInterval = setInterval(() => this.pollForUpdates(), 10000);
            }
        ).catch((error) => {
            handleErrors(error);
        })
    }

    componentWillUnmount() {
        clearInterval(this.pollingInterval);
    }

    createRootMessage(tid) {
        let root_threads = this.state.rootThreads.slice();
        root_threads.push(this.craftExchangeMessage(true, getUserData().displayName, tid, "create", null, null,
            false, true));

        this.setState({rootThreads: root_threads});
    }

    injectRootMessage(tid, content, owner) {
        let root_threads = this.state.rootThreads.slice();
        root_threads.push(this.craftExchangeMessage(true, owner, tid, "view", content, null, false, true));

        this.setState({rootThreads: root_threads});
    }

    getUnusedId() {
        let number = Math.floor(Math.random() * Math.floor(100000000));
        while (this.all_threads.has(number)) {
            number = Math.floor(Math.random() * Math.floor(100000000));
        }
        return number;
    }

    render() {
        return <div>
            {this.getRootThreads()}
            <hr/>
            <div className={"text-center"}>
                <Button variant={"primary"}
                        onClick={this.createRootMessage.bind(this, this.getUnusedId())}>
                    Add root thread
                </Button>
            </div>
        </div>;
    }

}

export default ExchangeBoard;