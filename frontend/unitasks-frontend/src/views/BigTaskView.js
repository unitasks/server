import React, {Component} from "react";
import TaskView from "../fragments/task_view";


class BigTaskView extends Component {

    render() {
        return <div className={"container"}><TaskView task_id={this.props.match.params.task_id}/></div>;
    }

}

export default BigTaskView;