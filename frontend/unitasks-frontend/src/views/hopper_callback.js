import React, {Component} from "react";

import axios from "axios";
import {getToken} from "../util/cookie_manager";
import querySearch from "stringquery";



class HopperCallback extends Component {

    constructor(props) {
        super(props);

        this.state = {};

        const parsed = querySearch(location.search);

        let status = parsed.status;
        let id = parsed.id;


        if(status === "success") {
            this.state.initialized = false;
            this.state.subscriptionId = id;

            axios.post("/api/hopper/setSubscription/"+id, {},
                { headers: { Authorization: "Bearer " +getToken() }}
                ).then((response) => {
                    this.setState({
                        initialized: true
                    });
            }).catch((error) => {
                console.error(error);
            });

        }else {
            this.state.initialized = true;
            this.state.error = true;
        }

    }

    render() {
        if(!this.state.initialized) {
            return <h3>Loading...</h3>;
        }else {
            if (this.state.error) {
                return <h3 className={"ml-3 mt-2"}>An error occurred</h3>;
            }else {
                return <h3 className={"ml-3 mt-2"}>Successfully connected to hopper!</h3>;
            }
        }
    }

}

export default HopperCallback;