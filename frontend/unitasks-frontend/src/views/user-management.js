import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import {Button} from "react-bootstrap";
import {checkEmail, checkPassword, checkUsername} from "../util/validator";
import axios from "axios";
import {toast} from "react-toastify";
import {getToken} from "../util/cookie_manager";
import Alert from "react-bootstrap/Alert";
import {handleErrors} from "../util/error_manager";
import {getInstance} from "../util/event_system";


class UserManagement extends Component {

    constructor(props) {
        super(props);

        this.validations = {
            "email": false,
            "displayname": false,
            "password": false,
            "new_password": false,
            "new_password_matches": false
        };
        this.validationsOld = {
            "email": true,
            "password": true,
            "new_password_matches": true,
            "displayname":true
        };

        this.usernameRef = React.createRef();
        this.emailRef = React.createRef();
        this.displaynameRef = React.createRef();
        this.passwordRef = React.createRef();
        this.newPasswordRef = React.createRef();
        this.newPasswordRepeatRef = React.createRef();

        this.state = {
            saveButtonEnabled: false,
            cancelButtonEnabled: false,
            emailConfirmedText: "Email unconfirmed",
            emailConfirmedColor: "red",
            showDialog: false,
            hopper_done_loading: false,
            hopper_added: false
        };
        this.loadData();
    }

    loadData(){
        axios.get("/api/member", { "headers": { "Authorization": "Bearer " +getToken() } })
            .then((res) => {
                this.emailRef.current.value = res.data.email;
                this.usernameRef.current.value = res.data.username;
                this.displaynameRef.current.value = res.data.displayname;
                this.validations["email"] = true;
                this.validations["username"]=true;

                if(res.data.displayname !== "Not set") {
                    this.validations["displayname"] = true;
                }

                if(res.data.emailconfirmed){
                    this.setState({emailConfirmedColor: "green", emailConfirmedText: "Email confirmed"});
                }else{
                    this.setState({emailConfirmedColor: "red", emailConfirmedText: "Email unconfirmed"});
                }

                axios.get("/api/hopper/isSubscribed", {
                    headers: {
                        Authorization: 'Bearer ' + getToken()
                    }
                }).then(
                    (response) => {
                        let subscribed = response.data.subscribed;

                        this.setState({
                            hopper_added: subscribed,
                            hopper_done_loading: true
                        });
                    }
                );
            }).catch((error) => {
                handleErrors(error);
            });

    }

    onHopperButtonClick() {
        axios.get("/api/hopper/getUrl", {
            headers: {
                Authorization: "Bearer " + getToken()
            }
        }).then(
            (response) => {
                window.location = response.data.url;
            }
        )
    }

    getHopperButton() {
        if (!this.state.hopper_done_loading) {
            return <div/>;
        } else {
            if (!this.state.hopper_added) {
                return <Button onClick={this.onHopperButtonClick.bind(this)} variant={"info"} className={"mr-sm-2"}>Add
                    Hopper notifications</Button>;
            } else{ return <div/>;}
        }
    }

    showMessage(key,msg){
        if(this.validations[key]!==this.validationsOld[key]){
            if(!this.validations[key]) {
                toast.error(msg);
            }
            this.validationsOld[key] = this.validations[key];
        }
    }

    allValidated() {
        if(!(this.validations["email"]  && this.validations["displayname"])){
            return false;
        }
        if(this.newPasswordRef.current.value === ""){
            if(this.passwordRef.current.value !== "" || this.newPasswordRepeatRef.current.value !== ""){
                return false;
            }
        }else{
            if(!(this.validations["new_password"] && this.validations["password"])){
                return false;
            }
            if(!this.validations["new_password_matches"]){
                return false;
            }
        }
        return true;
    }

    handleChange(event) {
        switch (event.target.id ) {
            case "formEmail":
                this.validations["email"] = checkEmail(this.emailRef.current.value);
                this.showMessage("email","Please enter a correct email.");
                this.setState({emailConfirmedColor: "red", emailConfirmedText: "Email unconfirmed"});
                break;
            case "formDisplayname":
                this.validations["displayname"] = checkUsername(this.displaynameRef.current.value);
                this.showMessage("displayname","A display name must consist of 5-32 characters. Letters, numbers and underscores are allowed.");
                break;
            case "formPassword":
                this.validations["password"] = checkPassword(this.passwordRef.current.value);
                break;
            case "formNewPassword":
                this.validations["new_password"] = checkPassword(this.newPasswordRef.current.value);
                this.showMessage("password","A password must contain at least 5 characters.");
                break;
            case "formRepeatNewPassword":
                this.validations["new_password_matches"] = this.newPasswordRef.current.value === this.newPasswordRepeatRef.current.value;
                this.showMessage("new_password_matches","Your passwords do not match.");
                break;
        }
        this.setState({ saveButtonEnabled: this.allValidated(), cancelButtonEnabled: true });
    }

    pressSaveButton() {
       let userdata;
       if(this.newPasswordRef.current.value !== ""){
            userdata = {
                email: this.emailRef.current.value,
                displayname: this.displaynameRef.current.value,
                password: this.newPasswordRef.current.value
            };
       }else{
            userdata = {
                email: this.emailRef.current.value,
                displayname: this.displaynameRef.current.value
            };
       }

        axios.put("/api/member", userdata,
            { headers: { Authorization: "Bearer " +getToken() }})
            .then((response) => {
                toast.success("Update of userdata successful!");

                if(this.newPasswordRef.current.value !== ""){
                    this.passwordRef.current.value = "";
                    this.newPasswordRef.current.value = "";
                    this.newPasswordRepeatRef.current.value = "";
                }
            }).catch((error) => {
                handleErrors(error);
            });
        }

    deleteUser() {
        this.setState({showDialog:false});

        axios.delete("api/member", { headers: { Authorization: "Bearer " +getToken() }})
            .then((res) => {
                toast.success("User successfully deleted!");
                getInstance().triggerEvent("login");
            });
        this.props.history.push("/login");
    }
    
    cancelEditing(){
        this.setState({ saveButtonEnabled:false, cancelButtonEnabled: false});
        this.loadData();
        this.passwordRef.current.value = "";
        this.newPasswordRef.current.value = "";
        this.newPasswordRepeatRef.current.value = "";
    }

    render() {
        return (
            <div className={"container"}>
                <h1>User Management</h1>
                <Alert show={this.state.showDialog} variant="danger">
                    <Alert.Heading>Delete user</Alert.Heading>
                    <p>
                        Please confirm the deletion of your user. This cannot be undone.
                    </p>
                    <div className="d-flex justify-content-end">
                        <Button onClick={(() => this.deleteUser())} variant="outline-danger">
                            Delete user
                        </Button>
                        <Button onClick={() => this.setState({showDialog:false})} className={"ml-2"} variant="outline-secondary">
                            Cancel
                        </Button>
                    </div>
                </Alert>
                {this.getHopperButton()}
                <Form className={"my-4"}>
                    <Form.Group controlId={"formUsername"}>
                        <Form.Control type={"text"} ref={this.usernameRef} placeholder={"Username"}
                                      disabled/>
                    </Form.Group>

                    <Form.Group controlId={"formEmail"}>
                        <Form.Control type={"text"} ref={this.emailRef} placeholder={"Email"}
                                      onChange={(e) => this.handleChange(e)}/>
                        <Form.Control plaintext style={{color:this.state.emailConfirmedColor}} readOnly value={this.state.emailConfirmedText} />
                    </Form.Group>

                    <Form.Group controlId={"formDisplayname"}>
                        <Form.Control type={"text"} ref={this.displaynameRef} placeholder={"Displayname"}
                                      onChange={((e) => this.handleChange(e))}/>
                    </Form.Group>

                    <Form.Group controlId={"formPassword"}>
                        <Form.Control type={"password"} ref={this.passwordRef} placeholder={"Current Password"}
                                      onChange={((e) => this.handleChange(e))}/>
                    </Form.Group>

                    <Form.Group controlId={"formNewPassword"}>
                        <Form.Control type={"password"} ref={this.newPasswordRef} placeholder={"New Password"}
                                      onChange={((e) => this.handleChange(e))}/>
                    </Form.Group>

                    <Form.Group controlId={"formRepeatNewPassword"}>
                        <Form.Control type={"password"} ref={this.newPasswordRepeatRef}
                                      placeholder={"Confirm new Password"} onChange={((e) => this.handleChange(e))}/>
                    </Form.Group>

                    <Button disabled={!this.state.saveButtonEnabled} variant={"primary"}
                            className={"mr-2"}  onClick={() => this.pressSaveButton()}>
                        Save
                    </Button>
                    <Button variant={"outline-danger"} className={"mr-2"} onClick={() => this.setState({showDialog: true})}>
                        Delete
                    </Button>
                    <Button style={{float: "right"}} disabled={!this.state.cancelButtonEnabled} variant={"secondary"} className={"mr-2 float-right"}
                             onClick={() => this.cancelEditing()}>
                        Cancel
                    </Button>

                </Form>

            </div>
        );

    }
}

export default UserManagement;