import React, {Component} from "react";
import TaskEditor from "./task";

class CreateTask extends Component{

    render() {
        return <TaskEditor is_subtask={false}/>;
    }

}


export default CreateTask;