import React, { Component } from "react";
import { Button } from "react-bootstrap";
import axios from "axios";
import { toast } from "react-toastify";
import { getToken } from "../util/cookie_manager";
import Alert from "react-bootstrap/Alert";
import GroupAccordion from "../fragments/group_accordion";
import Accordion from "react-bootstrap/Accordion";
import { handleErrors } from "../util/error_manager";

class GroupManagement extends Component {
    state = {
        groups: undefined,
        showDialog: false,
        selectedGroupName: "",
        selectedGroupId: undefined,
        initialized: false
    }

    getGroups() {
        let joinToken = this.props.match.params.join_token;

        if (joinToken !== undefined) {
            axios.get("/api/userInfo", {
                headers: {
                    Authorization: "Bearer " + getToken()
                }
            }).then((response) => {
                axios.post("/api/group/join/" + joinToken, null, { "headers": { "Authorization": "Bearer " + getToken() } })
                    .then((res) => {
                        toast.success("Group successfully joined!");
                        this.props.history.push("/group");
                    }).catch((error) => {
                        handleErrors(error);
                        this.props.history.push("/group");
                    });
            }).catch((error) => {
                toast.error("You need to be logged in to join a group!");
                this.props.history.push("/loginWithJoinToken/" + joinToken);
            });
        } else {
            axios.get("/api/groups", { "headers": { "Authorization": "Bearer " + getToken() } })
                .then((res) => {
                    this.setState({ groups: res.data });
                }).catch((error) => {
                    handleErrors(error);
                }).finally(() => {
                    this.setState({
                        initialized: true
                    });
                });
        }
    }

    editGroup = (id) => {
        this.props.history.push("/editGroup/" + id);
    }

    showDialog = (name, id) => {
        this.setState({ showDialog: true, selectedGroupName: name, selectedGroupId: id });
    }

    leaveGroup() {
        axios.delete("/api/group/" + this.state.selectedGroupId + "/leave", { headers: { Authorization: "Bearer " + getToken() } })
            .then(res => {
                toast.success("Group successfully left!");
                this.setState({ initialized: false, showDialog: false });
            }).catch((error) => {
                this.setState({ showDialog: false });
                handleErrors(error);
            });
    }

    render() {
        if (!this.state.initialized) {
            this.getGroups();
            return <div>Loading...</div>;
        } else {
            return (
                <div className={"container"}>
                    <h1>Group Management</h1>
                    <Alert show={this.state.showDialog} variant="danger">
                        <Alert.Heading>Leave group</Alert.Heading>
                        <p>
                            Are you sure you want to leave the group "{this.state.selectedGroupName}"?
                        </p>
                        <div className="d-flex justify-content-end">
                            <Button onClick={(() => this.leaveGroup())} variant="outline-danger">
                                Leave Group
                            </Button>
                            <Button onClick={() => this.setState({ showDialog: false })} className={"ml-2"}
                                variant="outline-secondary">
                                Cancel
                            </Button>
                        </div>
                    </Alert>
                    <Button className={"mb-2"} onClick={(() => this.props.history.push("/addGroup/"))}
                        variant={"primary"}>Create new Group</Button>
                    <Accordion>
                        <GroupAccordion groups={this.state.groups} edit={this.editGroup}
                            leave={this.showDialog}></GroupAccordion>
                    </Accordion>
                </div>
            );
        }
    }
}

export default GroupManagement;