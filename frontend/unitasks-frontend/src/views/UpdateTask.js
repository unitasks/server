import React, {Component} from "react";
import TaskEditor from "./task";

class UpdateTask extends Component{

    render() {
        return <TaskEditor is_subtask={false} task_id={this.props.match.params.task_id}/>;
    }

}

export default UpdateTask;