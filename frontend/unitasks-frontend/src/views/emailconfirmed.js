import React, {Component} from "react";

class EmailConfirmed extends Component {

    constructor(props) {
        super(props);

        let success = false;
        if (this.props.match.url.includes("emailConfirmed")) {
            this.success = true;
        }
    }

    render() {
        if (this.success) {
            return <h3 className={"ml-3 mt-2"}>Email confirmed!</h3>;
        }else {
            return <h3 className={"ml-3 mt-2"}>Error while confirming your email.</h3>;
        }
    }

}

export default EmailConfirmed;