import React, {Component} from "react";
import TaskList from "../fragments/task_list";
import {Button} from "react-bootstrap";


class TaskOverview extends Component{

    render() {
        return <div className={"container"}>
            <h1>Task List</h1>
              <Button className={"mb-2 mt-2"} onClick={() => (window.location.href="/addTask")}
                variant={"primary"}>Create new Task</Button>
            <TaskList is_root={true}/>
        </div>;
    }

}

export default TaskOverview;