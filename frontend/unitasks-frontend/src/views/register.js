import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import {Button} from "react-bootstrap";
import {checkEmail, checkPassword, checkUsername} from "../util/validator";
import axios from "axios";
import {toast} from "react-toastify";
import {handleErrors} from "../util/error_manager";

class RegisterPage extends Component {

    constructor(props) {
        super(props);
        this.emailRef = React.createRef();
        this.usernameRef = React.createRef();
        this.passwordRef = React.createRef();
        this.passwordRepeatRef = React.createRef();
        this.displaynameRef = React.createRef();
    }
    

    register() {
        if(this.validateInputs()) {
            axios.post("/api/register", {
                email: this.emailRef.current.value,
                username: this.usernameRef.current.value,
                password: this.passwordRef.current.value,
                displayname: this.displaynameRef.current.value
            }).then((response) => {
                toast.success("Registration successful!");
                this.props.history.push("/login");
            }).catch((error) => {
                handleErrors(error);
            });
        }
    }

    validateInputs(){
       if(!checkEmail(this.emailRef.current.value)){
           toast.error("Please enter a correct email.");
           return false;
       }
       if(!checkUsername(this.usernameRef.current.value)){
           toast.error("A username must consist of 5-32 characters. Letters, numbers and underscores are allowed.");
           return false;
       }
       if(!checkUsername(this.displaynameRef.current.value)){
           toast.error("A displayname must consist of 5-32 characters. Letters, numbers and underscores are allowed.");
           return false;
       }
       if(!checkPassword(this.passwordRef.current.value)){
           toast.error("A password must contain at least 5 characters.");
           return false;
       }
       if(!(this.passwordRef.current.value === this.passwordRepeatRef.current.value)){
           toast.error("Your passwords do not match.");
           return false;
       }
       return true;
    }
    render() {
        return (
            <div className={"container"}>
                <h1>Register</h1>
                <Form className={"my-4"}>
                    <Form.Group controlId={"formEmail"}>
                        <Form.Control type={"email"} placeholder={"Email"} ref={this.emailRef}/>
                        <Form.Text className={"text-muted"}>
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId={"formUsername"}>
                        <Form.Control type={"text"} placeholder={"Username"} ref={this.usernameRef}/>
                        <Form.Text className={"text-muted"}/>
                    </Form.Group>

                    <Form.Group controlId={"formDisplayname"}>
                        <Form.Control type={"text"} placeholder={"Displayname"} ref={this.displaynameRef}/>
                    </Form.Group>

                    <Form.Group controlId={"formPassword"}>
                        <Form.Control type={"password"} ref={this.passwordRef} placeholder={"Password"}/>
                    </Form.Group>

                    <Form.Group controlId={"formRepeatPassword"}>
                        <Form.Control type={"password"} ref={this.passwordRepeatRef} placeholder={"Repeat Password"}/>
                    </Form.Group>

                    <Button onClick={this.register.bind(this)} variant={"primary"} className={"mr-2"}>
                        Submit
                    </Button>

                </Form>
            </div>
        );
    }
}

export default RegisterPage;