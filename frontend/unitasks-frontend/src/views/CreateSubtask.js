import React, {Component} from "react";
import TaskEditor from "./task";

class CreateSubtask extends Component{

    render() {
        return <TaskEditor is_subtask={true} parent_task={this.props.match.params.parent_task}/>;
    }

}

export default CreateSubtask;