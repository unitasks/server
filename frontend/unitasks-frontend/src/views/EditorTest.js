import React, {Component} from "react";
import ContentEditor from "../fragments/content_editor";
import ContentDisplay from "../fragments/content_display";



class EditorTest extends Component{

    constructor(props) {
        super(props);
        this.state = {entries: []};
    }

    handleClick(a) {
        let array = this.state.entries;
        array.push(a);
        this.setState({entries: array});
    }

    handleCancel() {

    }

    renderEntries() {
        let result = [];
        for(let i = 0; i < this.state.entries.length; i++) {
            result.push(<ContentDisplay content={JSON.parse(this.state.entries[i])}/>);
        }

        return <div>
            {result}
        </div>;
    }

    render() {
        return <div><ContentEditor onSave={this.handleClick.bind(this)} onCancel={this.handleCancel.bind(this)} /> <div>  {this.renderEntries()} </div></div>;
    }

}

export default EditorTest;