import React, {Component} from "react";
import TaskEditor from "./task";

class UpdateSubtask extends Component{

    render() {
        return <TaskEditor is_subtask={true} task_id={this.props.match.params.task_id}/>;
    }

}

export default UpdateSubtask;