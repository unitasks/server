import React from "react";
import ReactDOM from "react-dom";
import { getToken, hasTokenCookie } from "./util/cookie_manager";
import { getInstance } from "./util/cookie_manager";
import LoginPage from "./views/login";
import RegisterPage from "./views/register";
import "./index.css";
import UnitasksFrame from "./frame/UnitasksFrame";
import "bootstrap/dist/css/bootstrap.min.css";
import { Router, Route } from "react-router-dom";
import { Redirect } from "react-router";
import UserManagement from "./views/user-management";
import axios from "axios";

import EditorTest from "./views/EditorTest";
import ExchangeBoard from "./views/ExchangeBoard";
import GroupManagement from "./views/group-management";
import Group from "./views/group";
import FileAreaEditor from "./fragments/file_area_editor";
import TaskOverview from "./views/taskoverview";
import UpdateTask from "./views/UpdateTask";
import CreateTask from "./views/CreateTask";
import CreateSubtask from "./views/CreateSubtask";
import UpdateSubtask from "./views/UpdateSubtask";
import BigTaskView from "./views/BigTaskView";
import { createBrowserHistory } from "history";
import HopperCallback from "./views/hopper_callback";
import {handleErrors} from "./util/error_manager";
import EmailConfirmed from "./views/emailconfirmed";

export const history = createBrowserHistory();

export const authorisation = {
    isAuthenticated: hasTokenCookie(),

    login(user, password) {
        return new Promise((resolve, reject) => {
            axios.post("/api/auth", {
                "user": user,
                "password": password
            }).then((response) => {
                this.isAuthenticated = true;
                resolve(response);
            }).catch((error) => {
                reject(error);
                handleErrors(error);
            });
        });
    },

    logout() {
        return new Promise((resolve, reject) => {
            axios.post("/api/logout", {}, {
                headers: {
                    Authorization: "Bearer " + getToken()
                }
            }).then((response) => {
                this.isAuthenticated = false;
                resolve(response);
            }).catch((error) => {
                reject(error);
            });
        });
    }
};

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        authorisation.isAuthenticated === true
            ? <Component {...props} />
            : <Redirect to={{
                pathname: "/login",
                state: { from: props.location }
            }} />
    )} />
);

const routing =
    <UnitasksFrame>
        <Router history={history}>
            <Route exact path="/register" component={RegisterPage} />
            <Route exact path="/login" component={LoginPage} />
            <Route exact path="/loginWithJoinToken/:join_token" component={LoginPage} />
            <PrivateRoute exact path="/" component={TaskOverview} />
            <PrivateRoute exact path="/taskList" component={TaskOverview} />

            <PrivateRoute exact path="/member" component={UserManagement} />
            <PrivateRoute exact path="/addTask" component={CreateTask} />
            <PrivateRoute exact path="/editTask/:task_id" component={UpdateTask} />
            <PrivateRoute exact path="/addSubtask/:parent_task" component={CreateSubtask} />
            <PrivateRoute exact path="/editSubtask/:task_id" component={UpdateSubtask} />
            <PrivateRoute exact path="/uploadFile" component={FileAreaEditor} />
            <PrivateRoute exact path="/showTask/:task_id" component={BigTaskView} />

            <PrivateRoute exact path="/group" component={GroupManagement} />
            <PrivateRoute exact path="/editGroup/:id" component={Group} />
            <PrivateRoute exact path="/addGroup" component={Group} />
            <PrivateRoute exact path="/displayGroup/:id" component={Group} />
            <PrivateRoute exact path="/group/join/:join_token" component={GroupManagement} />

            <PrivateRoute exact path="/testEditor" component={EditorTest} />
            <PrivateRoute exact path="/testBoard" component={ExchangeBoard} />

            <PrivateRoute exact path="/hopperCallback" component={HopperCallback} />
            <Route exact path="/emailConfirmed" component={EmailConfirmed} />
            <Route exact path="/emailError" component={EmailConfirmed}/>

        </Router>
    </UnitasksFrame>;

ReactDOM.render(routing,
    document.getElementById("root")
);