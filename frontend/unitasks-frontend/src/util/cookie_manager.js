import Cookies from "universal-cookie";

const cookies = new Cookies();

export function saveTokenCookie(value) {
    let d = new Date();
    d.setTime(d.getTime() + (14 * 24 * 60 * 60 * 1000));
    cookies.set("unitasks_token", value, { path: "*", expires: d });
}

export function clearTokenCookie() {
    cookies.remove("unitasks_token");
}

export function hasTokenCookie() {
    return getToken() !== undefined;
}

export function getToken() {
    return cookies.get("unitasks_token");
}
