const emailRegExp = /^[a-zA-Z_.0-9]+@[a-zA-Z_-]+?\.[a-zA-Z]{2,3}$/;

const usernameRegExp = /^[a-zA-Z_0-9]{5,32}/;

const passwordRegExp = /^.{5,}/;

export function checkEmail(email) {
    return emailRegExp.test(email);
}

export function checkUsername(username) {
    return usernameRegExp.test(username);
}

export function checkPassword(password) {
    return passwordRegExp.test(password);
}