import {toast} from "react-toastify";

export function handleErrors(error){
    if (error.response) {
        if(error.response.status === 400) {
            if(error.response.data !== undefined){
               toast.error(error.response.data);
            }
        }else if (error.response.status === 404) {
            toast.error("API not reachable! Are you in developer mode?");
        }
    } else {
        console.log(error.response);
        toast.error("Unclassifiable error occurred! Look into the console for more info!");
    }
}

export function handleErrorsMsg(error, msg){
    handleErrors(error);
    console.log(msg);
}