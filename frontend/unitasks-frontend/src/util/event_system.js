let instance;

export function getInstance() {
    if (instance) {
        return instance;
    } else {
        instance = new EventSystem();
        return instance;
    }
}

class EventSystem {

    constructor() {
        this.events = new Map();
    }

    registerListener(eventName, callback) {
        if (!this.events.has(eventName)) {
            this.events.set(eventName, new Map());
        }
        let event_key = new Date().valueOf();
        this.events.get(eventName).set(event_key, callback);

        return event_key;
    }

    removeListener(eventName, eventKey) {
        this.events.get(eventName).delete(eventKey);
    }

    triggerEvent(eventName) {
        let events = this.events.get(eventName);

        for (const [key, value] of events) {
            try {
                value.apply();
            } catch (e) {
                //console.error("Event handler failed!");
            }
        }
    }
}
