import React, {Component} from "react";
import {Image, Nav, Navbar} from "react-bootstrap";
import logo from "../img/img_logo_logo_full.svg";
import {ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import HeaderArea from "./HeaderArea";

export default class UnitasksFrame extends Component {

    render() {
        return <div><Navbar bg={"light"} variant={"light"}>
            <Navbar.Brand href={"/"}>
                <Image src={logo} height={"30"}/>
            </Navbar.Brand>
            <Nav className={"mr-auto"}/>
            <HeaderArea/>
        </Navbar>

            {this.props.children}

            <ToastContainer
                position={"top-center"}
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={true}
                closeOnClick={true}
                rtl={false}
                pauseOnFocusLoss={true}
                draggable={true}
                pauseOnHover={true}
            />
        </div>;
    }
}