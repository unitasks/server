import React, { Component } from "react";
import axios from "axios";
import { getToken, clearTokenCookie } from "../util/cookie_manager";
import { Button, Form } from "react-bootstrap";
import { getInstance } from "../util/event_system";
import { toast } from "react-toastify";
import { setUserData } from "../util/user_data";
import { history, authorisation } from "../index";

export default class HeaderArea extends Component {

    constructor(props) {
        super(props);
        this.state = {
            doneLoading: false
        };
        this.userData = null;

        this.getHeaderContent();
        this.login_event_key = getInstance().registerListener("login", this.notifyChange.bind(this));
    }

    notifyChange() {
        this.getHeaderContent();
    }

    getHeaderContent() {
        axios.get("/api/userInfo", {
            headers: {
                Authorization: "Bearer " + getToken()
            }
        }).then((response) => {
            let username = response.data.username;
            let email = response.data.email;
            let displayName = response.data.displayName;

            this.userData = {
                username: username,
                email: email,
                displayName: displayName
            };

            setUserData(this.userData);
            this.setState({ doneLoading: true });
        }).catch((error) => {
            this.userData = null;
            this.setState({ doneLoading: true });
        });
    }

    logout() {
        authorisation.logout()
            .then((response) => {
                this.userData = null;
                clearTokenCookie();
                this.setState({ doneLoading: true });
                toast.info("Logged out!");
                history.push("/login");
            })
            .catch((error) => {
                if (error.response) {
                    if (error.response.status === 400) {
                        toast.error("Logout failed! - Did you logout when already logged out in another tab?");
                    } else if (error.response.status === 404) {
                        toast.error("API not reachable! Are you in developer mode?");
                    }
                } else {
                    toast.error("Unclassifiable error occurred! Look into the console for more info!");
                }
            });
    }

    componentWillUnmount() {
        getInstance().removeListener("login", this.login_event_key);
    }

    render() {
        if (!this.state.doneLoading) {
            return <div>Loading...</div>;
        } else if (this.state.doneLoading) {
            if (this.userData === null) {
                return <Form inline>
                    <Button href={"/login"} variant={"outline-primary"} className={"mr-sm-2"}>Login</Button>
                    <Button href={"/register"} variant={"outline-success"}>Register</Button>
                </Form>;
            } else {
                return <Form inline>
                    <Button href={"/taskList"} variant={"outline-secondary"} className={"mr-sm-2"}>Tasks</Button>
                    <Button href={"/group"} variant={"outline-secondary"} className={"mr-sm-2"}>Groups</Button>
                    <Button href={"/member"} variant={"outline-secondary"}
                        className={"mr-sm-2"}>{this.userData.username}</Button>
                    <Button onClick={this.logout.bind(this)} variant={"outline-secondary"}>Logout</Button>
                </Form>;
            }
        }
    }

}