import React, {Component} from "react";
import {EditorState, convertToRaw, convertFromRaw} from "draft-js";
import {Editor} from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import {Button} from "react-bootstrap";
import FileAreaEditor from "./file_area_editor";


class ContentEditor extends Component {

    constructor(props) {
        super(props);

        let content_;
        if (props.content === null) {
            content_ = {text: EditorState.createEmpty(), files: []};
        } else {
            content_ = {
                text: EditorState.createWithContent(convertFromRaw(props.content.text)),
                files: props.content.files
            };
        }

        this.state = {
            editorState: content_.text,
            fileState: content_.files
        };

        this.filesRef = React.createRef();
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
    };

    onSaveHandler() {
        let textState = convertToRaw(this.state.editorState.getCurrentContent());
        let fileState = this.filesRef.current.getFiles();

        let combinedState = {
            text: textState,
            files: fileState
        };

        this.props.onSave(combinedState);
    }

    onCancelHandler() {
        this.props.onCancel();
    }

    render() {
        const {editorState} = this.state;
        const {fileState} = this.state;
        return (
            <div>
                <div style={{
                    marginLeft: "10px",
                    backgroundColor: "#eaeaea",
                    borderRadius: "10px",
                    border: "0px solid #000",
                    padding: "5px"
                }}>
                    <Editor
                        editorState={editorState}
                        wrapperClassName="wrapper"
                        editorClassName="editor"
                        onEditorStateChange={this.onEditorStateChange}
                    />
                </div>
                <FileAreaEditor files={fileState} ref={this.filesRef}/>
                <div className={"text-right"}>
                    <Button variant={"outline-danger"} className={"mr-sm-2"} onClick={this.onCancelHandler.bind(this)}>
                        Cancel
                    </Button>
                    <Button variant={"success"} onClick={this.onSaveHandler.bind(this)}>
                        Save
                    </Button>
                </div>
            </div>
        );
    }
}

export default ContentEditor;