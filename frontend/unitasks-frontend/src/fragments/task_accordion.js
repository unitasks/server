import React, {Component} from "react";
import {Accordion, Card} from "react-bootstrap";
import TaskElement from "./task_element";


class TaskAccordion extends Component{


    render() {
        return <Accordion>
            <TaskElement cardTitle={"Test1"} is_pending={true}/>
            <TaskElement cardTitle={"Test2"} is_pending={true}/>
            <TaskElement cardTitle={"Test3"} is_pending={true}/>
        </Accordion>;
    }

}

export default TaskAccordion;