import React, {Component} from "react";
import draftToHtml from "draftjs-to-html";
import FileAreaDisplay from "./file_area_display";


class ContentDisplay extends Component{

    render() {
        return <div>
            <div style={{
                backgroundColor: "#f9f9f9",
                marginLeft: "5px",
                paddingLeft: "10px",
                borderRadius: "10px",
                border: "0px solid #000"
            }} dangerouslySetInnerHTML={{__html: draftToHtml(this.props.content.text)}}/>
            <div style={{
                marginTop: "10px",
                marginLeft: "10px",
                marginBottom: "10px"
            }}>
                <FileAreaDisplay files={this.props.content.files}/>
            </div>
        </div>;
    }

}

export default ContentDisplay;
