import React, {Component} from "react";
import axios from "axios";
import {getToken} from "../util/cookie_manager";
import FileAreaDisplay from "./file_area_display";
import Button from "react-bootstrap/Button";


class FileAreaEditor extends Component {

    constructor(props) {
        super(props);

        this.inputRef = React.createRef();

        this.state = {
            files: props.files === null ? [] : props.files,
            uploading: false
        };

    }

    onFileChange() {
        this.setState({selectedFile: this.inputRef.current.files[0]});
    }

    getFiles() {
        return this.state.files;
    }

    onFileUpload() {
        const formData = new FormData();

        formData.append("file", this.state.selectedFile, this.state.selectedFile.name);

        this.setState({uploading: true});

        axios.post("/api/file", formData, {
            headers: {
                Authorization: "Bearer " + getToken()
            }
        }).then(
            (response) => {
                let filesArray = this.state.files;

                this.setState({uploading: false});

                filesArray.push({fileId: response.data.fileId, name: this.state.selectedFile.name});
                this.setState({
                    files: filesArray
                });
            }
        ).catch(
            (error) => {
                this.setState({uploading: false});
            }
        );

    }

    getUploadButtonArea() {
        if(!this.state.uploading) {
            return <div style={{margin: "10px"}}>
                <input type={"file"} ref={this.inputRef} onChange={this.onFileChange.bind(this)}/>
                <Button variant={"primary"} className={"ml-2"} onClick={this.onFileUpload.bind(this)}>Upload!</Button>
            </div>;
        }else {
            return <div style={{margin: "10px"}}>
                Uploading...<br/>
                Please wait until the upload is finished before you save!
            </div>
        }
    }

    render() {
        return <div style={{
            marginTop: "10px",
            marginLeft: "10px",
            marginBottom: "10px"
        }}>
            <FileAreaDisplay files={this.getFiles()}/>
            {this.getUploadButtonArea()}
        </div>;
    }

}

export default FileAreaEditor;