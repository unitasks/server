import React, {Component} from "react";
import File from "./file";
import {ListGroup} from "react-bootstrap";


class FileAreaDisplay extends Component {

    constructor(props) {
        super(props);
    }

    getFiles() {
        let fileList = [];
        for (let i = 0; i < this.props.files.length; i++) {
            let currentFile = this.props.files[i];
            fileList.push(<File fileId={currentFile.fileId} fileName={currentFile.name}/>);
        }

        return fileList;
    }

    render() {
        if (this.props.files.length > 0) {
            return <div>
                <strong>Attached files:</strong>
                <ListGroup>
                    {this.getFiles()}
                </ListGroup></div>;
        } else {return <div/>;}
    }

}

export default FileAreaDisplay;