import React, {Component} from "react";
import {Card, Button} from "react-bootstrap";
import Collapse from "react-bootstrap/Collapse";
import GroupHeader from "./group_header";


class GroupElement extends Component {

    constructor(props) {
        super(props);

        this.state = {
            is_open: false
        };
    }

    render() {
        const {description, membercount} = this.props.group;
        return <Card>
            <Card.Header>
                <GroupHeader parent={this} group={this.props.group} edit={this.props.edit} leave={this.props.leave} eventKey={this.props.eventKey}/>
            </Card.Header>
            <Collapse in={this.state.is_open}>
                <div>
                    <Card.Body>
                        <p>{description}</p>
                        <p>{membercount} members</p>
                    </Card.Body>
                </div>
            </Collapse>
        </Card>;
    }

}

export default GroupElement;