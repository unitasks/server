import React, {Component} from "react";
import {Accordion, Card, Button} from "react-bootstrap";
import TaskHeader from "./task_header";
import Collapse from "react-bootstrap/Collapse";
import TaskList from "./task_list";
import ContentDisplay from "./content_display";
import axios from "axios";
import {getToken} from "../util/cookie_manager";
import {history} from "../index";
import {handleErrors} from "../util/error_manager";


class TaskElement extends Component {

    constructor(props) {
        super(props);

        this.state = {
            is_open: false,
            is_pending: props.is_pending
        };
    }

    change_status() {
        let initialStatus = this.state.is_pending;

        let newState;
        if (initialStatus) {
            newState = "done";
        } else {
            newState = "pending";
        }

        axios.put("/api/task/set_state/" + this.props.task_id + "/" + newState, {}, {
            headers: {
                Authorization: "Bearer " + getToken()
            }
        }).then(
            (response) => {
                this.setState({
                    is_pending: !initialStatus
                });
            }
        ).catch(
            (error) => {
                handleErrors(error);
            }
        );
    }

    handleDelete() {
        if(this.props.editable) {
            axios.delete("/api/task/" + this.props.task_id, {
                headers: {
                    Authorization: "Bearer " + getToken()
                }
            }).then(
                (response) => {
                    this.props.task_list.remove_task(this.props.task_id);
                }
            ).catch(
                (error) => {
                    handleErrors(error);
                }
            );
        }
    }

    handleEdit() {
        if(this.props.editable) {
            if (this.props.is_subtask) {
                history.push("/editSubtask/" + this.props.task_id);
            } else {
                history.push("/editTask/" + this.props.task_id);
            }
        }
    }

    handleOpenTask() {
        history.push("/showTask/"+this.props.task_id);
    }

    renderSubtasks() {
        if (this.props.is_root) {
            return <div style={{
                marginLeft: "15px",
                marginRight: "15px"
            }}>
                <TaskList is_root={false} tasks={this.props.tasks}/>

                <div style={{margin: "10px"}} className={"text-center"}>
                    {this.props.editable ? <Button variant={"primary"} onClick={this.handleAddSubtask.bind(this)}>Add subtask</Button> : <div/>}
                </div>

            </div>;
        } else {return <div/>;}
    }

    handleAddSubtask() {
        history.push("/addSubtask/"+this.props.task_id);
    }

    getContext() {
        if(this.props.type === "group") {
            return <strong>Group task "{this.props.groupName}"</strong>;
        }else {return <strong>Personal task</strong>;}
    }

    getCardColor() {
        return this.props.type === "group" ? "rgba(0,0,255,0.03)" : "rgba(0,0,0,0.03)";
    }

    render() {
        return <Card>
            <Card.Header style={{
                backgroundColor: this.getCardColor()
            }}>
                <TaskHeader parent={this} eventKey={this.props.eventKey}
                            headerTitle={this.props.title} editable={this.props.editable}/>
            </Card.Header>
            <Collapse in={this.state.is_open} ref={this.collapseRef}>
                <div>
                    <Card.Body>
                        <div>{this.getContext()}</div>
                        <div style={{marginBottom: "10px"}}>Due to: {this.props.dueDate}</div>
                        <ContentDisplay content={JSON.parse(this.props.description)}/></Card.Body>
                    {this.renderSubtasks()}
                </div>
            </Collapse>
        </Card>;
    }

}

export default TaskElement;