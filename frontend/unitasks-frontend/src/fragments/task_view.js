import React, {Component} from "react";
import ContentDisplay from "./content_display";
import TaskList from "./task_list";
import ExchangeBoard from "../views/ExchangeBoard";
import axios from "axios";
import {getToken} from "../util/cookie_manager";
import {Button, Col, Container, Row} from "react-bootstrap";
import {history} from "../index";


class TaskView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            initialized: false
        };

    }

    loadData() {
        axios.get("/api/task/" + this.props.task_id, {
            headers: {
                Authorization: "Bearer " + getToken()
            }
        }).then(
            (response) => {
                this.setState({
                    description: JSON.parse(response.data.description),
                    title: response.data.title,
                    dueDate: response.data.datetime,
                    admin: response.data.admin,
                    type: response.data.type,
                    groupName: response.data.groupname
                });

                axios.get("/api/subtasks/" + response.data.taskid, {
                    headers: {
                        Authorization: "Bearer " + getToken()
                    }
                }).then(
                    (response) => {
                        this.setState({
                            subtasks: response.data,
                            initialized: true
                        });
                    }
                );

            }
        )

    }

    handleAddSubtask() {
        history.push("/addSubtask/" + this.props.task_id);
    }

    handleEditTask() {
        history.push("/editTask/" + this.props.task_id);
    }

    getContext() {
        if(this.state.type === "group") {
            return <strong>Group task "{this.state.groupName}"</strong>;
        }else{ return <strong>Personal task</strong>;}
    }

    render() {
        if (this.state.initialized) {
            return (
                <div>
                    <div style={{
                        marginBottom: "30px"
                    }}>
                        <Container fluid>
                            <Row>
                                <Col sm={10}>
                                    <div className={"text-center"}>
                                        <h1>{this.state.title}</h1>
                                    </div>
                                </Col>
                                <Col sm={2}>
                                    <div style={{
                                        marginTop: "10px"
                                    }}>
                                        {this.state.admin ?
                                            <Button onClick={this.handleEditTask.bind(this)} variant={"primary"}>
                                                Edit
                                            </Button> : <div/>
                                        }
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <div style={{margin: "10px"}}>{this.getContext()}</div>
                    <ContentDisplay content={this.state.description}/>
                    <h3>Subtasks:</h3>
                    <div>
                        <TaskList is_root={false} tasks={this.state.subtasks}/>
                        <div style={{margin: "10px"}} className={"text-center"}>
                            {this.state.admin ?
                                <Button variant={"primary"} onClick={this.handleAddSubtask.bind(this)}>
                                    Add subtask
                                </Button> : <div/>
                            }
                        </div>
                    </div>
                    <div className={"text-center"} style={{
                        marginTop: "30px"
                    }}><h2>ExchangeBoard:</h2></div>
                    <ExchangeBoard task_id={this.props.task_id}/>
                </div>
            );
        } else {
            this.loadData();
            return <div>Loading...</div>;
        }
    }

}

export default TaskView;
