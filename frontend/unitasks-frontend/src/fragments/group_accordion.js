import React, {Component} from "react";
import GroupElement from "./group_element";
import PropTypes from "prop-types";

class GroupAccordion extends Component{

    render() {
         return this.props.groups.map((group) =>(
            <GroupElement key={group.groupId} group={group} edit={this.props.edit} leave={this.props.leave} />
        ));
    }

}

GroupAccordion.propTypes = {
    groups: PropTypes.array.isRequired,
    edit: PropTypes.func.isRequired,
    leave:PropTypes.func.isRequired
};

export default GroupAccordion;