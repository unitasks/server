import React, {Component} from "react";
import axios from "axios";
import {getToken} from "../util/cookie_manager";
import TaskElement from "./task_element";
import {Accordion, Button} from "react-bootstrap";

class TaskList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tasks: [],
            initialized: false
        };
    }

    getTasks() {
        if(this.props.is_root) {
            axios.get("/api/tasks", {
                headers: {
                    Authorization: "Bearer " + getToken()
                }
            }).then(
                (response) => {
                    let tasks = response.data;

                    this.setState({
                        tasks: tasks
                    });
                }
            ).finally(
                () => {
                    this.setState({
                        initialized: true
                    });
                }
            );
        }else {
            this.setState({
                tasks: this.props.tasks,
                initialized: true
            });
        }
    }

    remove_task(taskId) {
        let taskList = [];

        for(let i = 0; i < this.state.tasks.length; i++) {
            if(taskId !== this.state.tasks[i].taskid) {
                taskList.push(this.state.tasks[i]);
            }
        }

        this.setState({
            tasks: taskList
        });
    }

    hasEditRights(task) {
        if(task.type === "personal") {
            return true;
        }else {
            return task.admin;
        }

    }

    renderAccordion() {
        let taskElements = [];

        for(let i = 0; i < this.state.tasks.length; i++) {
            let task = this.state.tasks[i];
            let is_pending = task.status === "pending";
            taskElements.push(<TaskElement is_root={this.props.is_root} title={task.title} description={task.description} dueDate={task.duedate} tasks={task.subtasks}
                               is_pending={is_pending} type={task.type} groupName={task.groupname} task_id={task.taskid} taskList={this} is_subtask={task.parent !== null} editable={this.hasEditRights(task)} />);
        }
        if(taskElements.length > 0) {
            return <Accordion>
                {taskElements}
            </Accordion>;
        }else {
            return <div className={"text-center"}>Tasklist empty</div>;
        }
    }



    render() {
        if(!this.state.initialized) {
            this.getTasks();
            return <div>Loading...</div>;
        }else {
            return <div className={"container"}>
                {this.renderAccordion()}
                </div>;
        }

    }

}

export default TaskList;