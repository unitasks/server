import React, {Component, useState} from "react";
import MaterialIcon from "material-icons-react";
import {Badge, Button, Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class TaskHeader extends Component {
    constructor(props) {
        super(props);
        this.parent = props.parent;
    }

    toggle() {
        this.parent.setState({is_open: !this.parent.state.is_open});
    }

    getBadgeVariant() {
        if (this.parent.state.is_pending) {
            return "warning";
        }else{ return "success";}
    }

    getBadgeContent() {
        if (this.parent.state.is_pending) {
            return "pending";
        }else{ return "done";}
    }

    getCheckboxIcon() {
        if (this.parent.state.is_pending) {
            return <MaterialIcon icon={"check_box_outline_blank"}/>;
        } else {
            return <MaterialIcon icon={"check_box"}/>;
        }
    }

    statusUpdate() {
        this.parent.change_status();
    }

    handleDelete() {
        this.parent.handleDelete();
    }

    handleEdit() {
        this.parent.handleEdit();
    }

    handleOpenTask() {
        this.parent.handleOpenTask();
    }

    getOpenTask() {
        if (this.parent.props.is_root) {
            return <a onClick={this.handleOpenTask.bind(this)} className={"ml-4"} style={{cursor: "pointer"}}>
                <div style={{marginTop: "9px", width: "24px", height: "24px"}}>
                    <MaterialIcon icon={"launch"}/>
                </div>
            </a>;
        } else {
            return <div/>;
        }
    }

    getPrivilegedControls() {
        if (this.props.editable) {
            return ([
                    <a onClick={this.handleEdit.bind(this)} className={"ml-4"} style={{cursor: "pointer"}}>
                        <div style={{marginTop: "9px", width: "24px", height: "24px"}}>
                            <MaterialIcon icon={"edit"}/>
                        </div>
                    </a>,
                    <a onClick={this.handleDelete.bind(this)} className={"ml-3"} style={{cursor: "pointer"}}>
                        <div style={{marginTop: "9px", width: "24px", height: "24px"}}>
                            <MaterialIcon icon={"delete"}/>
                        </div>
                    </a>]);
        }else {return <div/>;}
    }

    render() {
        return <div>
            <Container fluid>
                <Row>
                    <Col sm={9}>
                        <div>
                            <Button onClick={this.statusUpdate.bind(this)} variant={"text"} className={"mr-3"}>
                                <div style={{marginTop: "4px", width: "24px", height: "20px"}}>
                                    {this.getCheckboxIcon()}
                                </div>
                            </Button>
                            <Button variant={"text"} onClick={this.toggle.bind(this)}
                                    aria-expanded={this.parent.state.is_open}>
                                {this.props.headerTitle}
                            </Button>
                        </div>
                    </Col>
                    <Col sm={3}>
                        <div style={{display: "flex"}}>
                            <div style={{marginTop: "4px"}}><h5>
                                <Badge variant={this.getBadgeVariant()}>
                                    {this.getBadgeContent()}
                                </Badge>
                            </h5>
                            </div>
                            {this.getOpenTask()}
                            {this.getPrivilegedControls()}
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>;
    }

}

export default TaskHeader;