import React, {Component} from "react";
import {ListGroup} from "react-bootstrap";
import fileDownload from "js-file-download";
import axios from "axios";
import {getToken} from "../util/cookie_manager";



class File extends Component{

    constructor(props) {
        super(props);
    }

    downloadFile() {
        axios.get("/api/file/"+this.props.fileId, {
            headers: {
                Authorization: "Bearer " + getToken()
            },
            responseType: "arraybuffer"
        }).then(
            (response) => {
                let data = new Buffer.from(response.data, "binary");
                fileDownload(data, response.headers.filename);
            }
        );
    }

    render() {
        return <ListGroup.Item><a onClick={this.downloadFile.bind(this)}>{this.props.fileName}</a></ListGroup.Item>;
    }

}

export default File;