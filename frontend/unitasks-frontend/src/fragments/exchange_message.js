import React, {Component} from "react";
import ContentDisplay from "./content_display";
import ContentEditor from "./content_editor";
import MaterialIcon from "material-icons-react";
import {Button} from "react-bootstrap";
import axios from "axios";
import {getToken} from "../util/cookie_manager";
import {handleErrors, handleErrorsMsg} from "../util/error_manager";

class ExchangeThread extends Component {

    constructor(props) {
        super(props);
        this.message_id = props.message_id;
        this.editable = props.editable;
        this.owner = props.owner;
        this.is_root = props.is_root;
        this.exchange_board = props.exchange_board;
        this.parent = props.parent;

        this.state = {
            mode: props.mode,
            changing: props.changing,
            content: props.content,
            children: [],
            extended: false,
            has_children: props.has_children,
            deleted: false
        };
    }

    changeId(newId) {
        for (let child in this.state.children) {
            child.parent = newId;
        }
        this.exchange_board.changeIdInThreadRegistry(this.message_id, newId);
        this.message_id = newId;
    }

    addChild(child) {
        let children_ = this.state.children.slice();
        children_.push(child);

        this.setState({
            children: children_,
            has_children: true
        });
    }

    tryUpdating(content_) {
        this.setState({
            content: content_
        });
    }

    onEditorSave(content_) {
        if (this.state.changing && this.state.mode === "create") {

            let path;

            if (this.is_root) {
                path = "/api/task/" + this.exchange_board.task_id + "/message";
            } else {
                path = "/api/task/" + this.exchange_board.task_id + "/message/" + this.parent;
            }

            axios.post(path, content_, {
                headers: {
                    Authorization: "Bearer " + getToken()
                }
            }).then((response) => {
                    this.changeId(response.data.message_id);
                    this.setState({
                        content: content_,
                        changing: false,
                        mode: null
                    });
                }
            ).catch((error) => {
                handleErrorsMsg(error,"An error occurred while creating the entry!");
            })


        } else if (this.state.changing && this.state.mode === "edit") {

            axios.put("/api/task/" + this.exchange_board.task_id + "/message/" + this.message_id, content_, {
                headers: {
                    Authorization: "Bearer " + getToken()
                }
            }).then((response) => {
                this.setState({
                    content: content_,
                    changing: false,
                    mode: null
                })
            }).catch((error) => {
                handleErrorsMsg(error,"An error occurred while updating the entry!");
            })
        }

    }

    onEditorCancel() {
        if (this.state.mode === "create") {
            this.setState({
                deleted: true
            })
        } else if (this.state.mode === "edit") {
            this.setState({
                changing: false,
                mode: null
            })
        }
    }

    getChildren() {
        let childrenThreads = [];
        for (let i = 0; i < this.state.children.length; i++) {
            let child = this.state.children[i];
            childrenThreads.push(child);
        }

        return childrenThreads;
    }

    handleAdd() {
        if (this.state.has_children && !this.state.extended) {
            this.extendChildren();
        }
        this.setState({
            extended: true,
            has_children: true
        })
        this.exchange_board.createChildMessage(this.message_id, this.exchange_board.getUnusedId());
    }

    handleEdit() {
        this.setState({
            changing: true,
            mode: "edit"
        });
    }

    getMainContent() {
        if (this.state.changing) {
            return <ContentEditor onSave={this.onEditorSave.bind(this)} onCancel={this.onEditorCancel.bind(this)}
                                  content={this.state.content}/>;
        } else {
            return <div>
                <div className={"text-right"}>
                    {this.editable ? (
                            <Button onClick={this.handleEdit.bind(this)} variant={"text"}>
                                <MaterialIcon icon={"create"}/>
                            </Button>)
                        : null}
                    <Button onClick={this.handleAdd.bind(this)} variant={"text"}>
                        <MaterialIcon icon={"reply"}/>
                    </Button>
                </div>
                <div>
                    <ContentDisplay content={this.state.content}/>
                </div>
            </div>;
        }
    }

    retractChildren() {
        this.setState({
            extended: false
        });
        this.setState({
            children: []
        });
    }

    extendChildren() {
        axios.get("/api/task/" + this.exchange_board.task_id + "/message/" + this.message_id, {
            headers: {
                Authorization: "Bearer " + getToken()
            }
        }).then((response) => {
            let messages = response.data.messages;
            let children_ = [];

            for (let msg_index = 0; msg_index < messages.length; msg_index++) {
                let msg = messages[msg_index];
                children_.push(this.exchange_board.craftExchangeMessage(true, msg.owner_name, msg.message_id, "view", msg.content, this.message_id, msg.has_children, msg.editable));
            }
            this.setState({children: children_, has_children: true, extended: true});
        })
    }

    getFooter() {
        if (this.state.has_children) {
            if (this.state.extended === false) {
                return <Button variant={"info"} onClick={this.extendChildren.bind(this)}>Extend</Button>;
            } else if (this.state.extended === true) {
                return <Button variant={"dark"} onClick={this.retractChildren.bind(this)}>Retract</Button>;
            }
        } else {return null;}
    }

    renderChildren() {
        if (this.state.extended) {
            return <table style={{width: "100%"}}>
                <tr style={{width: "100%"}}>
                    <td style={{
                        padding: "0px",
                        width: "50px"
                    }}/>
                    <td style={{
                        width: "calc(100% - 50px)",
                        padding: "0px"
                    }}

                    >
                        <div style={{paddingLeft: "5px"}}>{this.getChildren()}</div>
                    </td>
                </tr>
            </table>;
        } else return null;
    }

    render() {
        if (!this.state.deleted) {
            return <div style={{
                //border: "1px solid #c4c4c4",
                //borderRadius: "5px",
                padding: "5px 5px 5px 0px",
                margin: "5px 5px 5px 0px"
            }}>
                <hr style={{border: "4px solid #ccc", borderRadius: "2px"}}/>
                <div style={{
                    marginLeft: "10px",
                    marginRight: "10px"
                }}>
                    <div style={{display: "flex"}}>
                        <div style={{marginTop: "5px"}}>
                            <MaterialIcon icon={"account_box"}/>
                        </div>
                        <strong style={{color: "#003fff"}}>
                            {this.owner}
                        </strong>
                    </div>
                    {this.getMainContent()}

                    {this.renderChildren()}
                    <hr/>
                    <div style={{
                        margin: "10px"
                    }} className={"text-center"}>
                        {this.getFooter()}
                    </div>
                </div>
            </div>;
        } else {return <div/>;}
    }

}

export default ExchangeThread;