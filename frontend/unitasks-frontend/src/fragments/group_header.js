import React, {Component, useState} from "react";
import MaterialIcon from "material-icons-react";
import {Button, Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import PropTypes from "prop-types";


class GroupHeader extends Component {
    constructor(props) {
        super(props);
        this.parent = props.parent;
    }

    toggle() {
        this.parent.setState({is_open: !this.parent.state.is_open});
    }

    render() {
        const {groupid, name, admin} = this.props.group;
        let editButton ="";
        let detailViewButton="";
        if(admin){
            editButton=<a onClick={this.props.edit.bind(this, groupid)} className={"ml-4"} style={{ cursor: "pointer"}} className={"ml-4"}>
                                <div style={{marginTop: "9px", width: "24px", height: "24px"}}>
                                    <MaterialIcon icon={"edit"}/>
                                </div>
                            </a>;
            detailViewButton=<a href={"/displayGroup/"+groupid} className={"ml-4"} style={{ cursor: "pointer"}}>
                <div style={{marginTop: "9px", width: "24px", height: "24px"}}>
                    <MaterialIcon icon={"launch"}/>
                </div>
            </a>;
        }
        return <div>
            <Container fluid>
                <Row>
                    <Col sm={9}>
                        <div>
                            <Button variant={"text"} onClick={this.toggle.bind(this)}
                                    aria-expanded={this.parent.state.is_open}>
                                {name}
                            </Button>
                        </div>
                    </Col>
                    <Col sm={3}>
                        <div style={{display: "flex"}}>
                            {detailViewButton}
                            {editButton}
                            <a onClick={this.props.leave.bind(this, name, groupid)} className={"ml-3"} style={{ cursor: "pointer"}}>
                                <div style={{marginTop: "9px", width: "24px", height: "24px"}}>
                                    <MaterialIcon icon={"person_remove"}/>
                                </div>
                            </a>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>;
    }

}

GroupHeader.propTypes = {
    edit: PropTypes.func.isRequired,
    leave:PropTypes.func.isRequired
};

export default GroupHeader;