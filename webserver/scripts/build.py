"""Compiles, minifies and gzips all files in src/static"""

# Requires that install.sh was run

import glob
import os
import subprocess

#
# Constants
#

SRC_DIR = os.path.dirname(os.path.realpath(__file__)) + "/.."
SRC_GLOB = "src/**/*."
BUILD_GLOB = "build/**/*."


#
# Functions
#

def get_files(ending: str, dir_glob: str):
    files = []
    print(dir_glob + ending)
    for file in glob.iglob(dir_glob + ending, recursive=True):
        if file.endswith(ending):
            files.append(os.path.abspath(file))
    return files


def build_files(ending: str, compressor: str):
    """Minifies and compresses the files with the specified ending."""
    print("\nBuilding {} files:".format(ending.upper()))
    for file in get_files(ending, SRC_GLOB):
        print("\tBuilding {}:".format(file.split("/")[-1]))
        gzip_file(
            minify_file(
                file,
                compressor
            )
        )


def minify_file(file: str, compressor: str) -> str:
    new_file = file.replace("src/", "build/", 1)
    print("\t\tMinifying.. ", end="")
    subprocess.check_output([
        "node-minify",
        "-i",
        file,
        "-o",
        new_file,
        "-c",
        compressor
    ],
        stderr=subprocess.DEVNULL
    )
    print("DONE")
    return new_file


def gzip_file(file: str):
    print("\t\tGzipping.. ", end="")
    subprocess.check_output([
        "gzip",
        file,
        "-k",
        "-9"
    ])
    print("DONE")


#
# Copy
#

def copy_dir(src: str, dest: str):
    directory = src.split("/")[-1]
    print("\nCopying directory {}.. ".format(directory), end="")
    subprocess.check_output([
        "cp",
        src,
        dest,
        "-r"
    ])
    print("DONE")
    for file in get_files("", dest + "/" + directory + "/**/*.*"):
        gzip_file(file)


def copy_files(ending: str):
    print("\nCopying {} files:".format(ending.upper()))
    files = get_files(ending, SRC_GLOB)
    for file in files:
        print("\tCopying {}.. ".format(file.split("/")[-1]), end="")
        new_file = file.replace("src", "build", 1)
        subprocess.check_output([
            "mkdir",
            "-p",
            "/".join(new_file.split("/")[:-1])
        ])
        subprocess.check_output([
            "cp",
            file,
            new_file
        ])
        print("DONE")


#
# Main
#

def main():
    #
    # Reset build
    #
    print("Resetting build: ", end="")
    try:
        subprocess.call([
            "rm",
            "-r",
            SRC_DIR + "/build"
        ], stderr=open(os.devnull, "wb"))
    # Build does not exist
    except subprocess.CalledProcessError:
        pass
    print("DONE")

    #
    # Build standard files
    #

    build_files("js", "babel-minify")
    build_files("css", "clean-css")
    build_files("html", "html-minifier")

    #
    # LESS Files
    #

    print("\nCompiling, minifying and gzipping LESS Files:")
    less_files = get_files("less", SRC_GLOB)
    for file in less_files:
        print("\t{}:".format(file.split("/")[-1]))

        # Compile
        print("\t\tCompiling.. ", end="")
        tmp_file = file.replace(".less", ".css-tmp")
        subprocess.check_output([
            "lessc",
            file,
            tmp_file
        ])
        print("DONE")

        # Minify
        print("\t\tMinifying.. ", end="")
        new_file = tmp_file.replace("src/", "build/", 1).replace(".css-tmp", ".css")
        subprocess.check_output([
            "node-minify",
            "-i",
            tmp_file,
            "-o",
            new_file,
            "-c",
            "clean-css"
        ],
            stderr=subprocess.DEVNULL
        )
        print("DONE")

        # GZipping
        gzip_file(new_file)

        # Delete
        print("\t\tDeleting artifacts.. ", end="")
        subprocess.check_output([
            "rm",
            tmp_file
        ])
        print("DONE")

    #
    # Copy Files
    #

    # IMG Files
    copy_dir(SRC_DIR + "/src/static/img", SRC_DIR + "/build/static", )

    # Other file types
    copy_files("py")
    copy_files("txt")
    copy_files("conf")
    copy_files("ini")
    copy_files("sh")
    copy_files("sql")

    print("\n\nDONE")


if __name__ == "__main__":
    main()
