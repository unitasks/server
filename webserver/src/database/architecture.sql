CREATE TABLE IF NOT EXISTS Member (
    userId serial PRIMARY KEY,
    email varchar(64) UNIQUE NOT NULL,
    emailConfirmed boolean DEFAULT FALSE,
    username varchar(64) UNIQUE NULL,
    password varchar (80) NOT NULL,
    displayName varchar(128) NOT NULL DEFAULT 'Not set',
    hopperSubId varchar DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS Token (
    tokenId serial PRIMARY KEY,
    token varchar(64) UNIQUE NOT NULL,
    userId int REFERENCES member (userId) ON DELETE CASCADE,
    timeout timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP + INTERVAL '48 hours'
);

CREATE TABLE IF NOT EXISTS UnitasksGroup (
    groupId serial PRIMARY KEY,
    name varchar(64) NOT NULL,
    description text
);

CREATE TABLE IF NOT EXISTS GroupJoinLink (
    groupId int REFERENCES UnitasksGroup (groupId) ON DELETE CASCADE,
    token varchar(32) UNIQUE NOT NULL,
    PRIMARY KEY (token, groupId)
);

CREATE TABLE IF NOT EXISTS GroupMember (
    groupId int REFERENCES UnitasksGroup (groupId) ON DELETE CASCADE,
    userId int REFERENCES member (userId) ON DELETE CASCADE,
    isGroupAdmin boolean,
    PRIMARY KEY (groupId, userId)
);

CREATE TABLE IF NOT EXISTS JWT_Token (
    tokenId serial PRIMARY KEY,
    userId int NOT NULL REFERENCES member (userId) ON DELETE CASCADE,
    token varchar(64) UNIQUE NOT NULL,
    timeout timestamp NOT NULL
);
DO $$ BEGIN
    CREATE TYPE taskType AS ENUM ('personal', 'group');
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS Task (
    taskId serial PRIMARY KEY,
    title varchar(128) NOT NULL,
    description text,
    raplaLink varchar(256),
    creationDate timestamp DEFAULT CURRENT_TIMESTAMP,
    dueDate timestamp DEFAULT CURRENT_TIMESTAMP,
    lastUpdated timestamp DEFAULT CURRENT_TIMESTAMP,
    createdBy int REFERENCES member (userId) ON DELETE CASCADE NOT NULL,
    type taskType NOT NULL
);

CREATE TABLE IF NOT EXISTS PersonalTask (
    personalTaskId int REFERENCES Task (taskId) ON DELETE CASCADE PRIMARY KEY,
    parent int REFERENCES PersonalTask (personalTaskId) ON DELETE CASCADE,
    userId int NOT NULL REFERENCES member (userId) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS GroupTask (
    groupTaskId int REFERENCES Task (taskId) ON DELETE CASCADE PRIMARY KEY,
    parent int REFERENCES GroupTask (groupTaskId) ON DELETE CASCADE,
    groupId int NOT NULL REFERENCES UnitasksGroup (groupId) ON DELETE CASCADE
);

DO $$ BEGIN
    CREATE TYPE taskStatusEnum AS ENUM ('done', 'in progress', 'pending');
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS TaskStatus (
    taskId int REFERENCES Task(taskId) ON DELETE CASCADE,
    userId int REFERENCES member(userId) ON DELETE CASCADE,
    status taskStatusEnum,
    PRIMARY KEY (taskId, userId)
);

CREATE TABLE IF NOT EXISTS ExchangeThreadMessage(
    messageId serial PRIMARY KEY,
    parentTask int REFERENCES task(taskId) ON DELETE CASCADE NOT NULL,
    parentMessage int REFERENCES ExchangeThreadMessage(messageId) ON DELETE CASCADE, -- NULL means its a root message
    userId int REFERENCES member(userId) ON DELETE CASCADE NOT NULL,
    postDate timestamp WITH TIME ZONE NOT NULL,
    lastEditedDate timestamp WITH TIME ZONE,
    content json NOT NULL
);

CREATE TABLE IF NOT EXISTS File(
    fileId serial PRIMARY KEY,
    fileName varchar(256),
    fileData bytea
);

CREATE TABLE IF NOT EXISTS HopperApp(
    appId int PRIMARY KEY,
    content jsonb unique NOT NULL
);

--
-- Views
--

CREATE OR REPLACE VIEW MemberTask AS
    SELECT
           taskStatus.userID, task.taskId, task.title, task.description,
           task.raplaLink, task.creationDate, task.dueDate,
           task.lastUpdated, task.createdBy, task.type,
           parent, status
    FROM task
        JOIN personalTask ON task.taskId = personalTask.personalTaskId
        JOIN taskStatus ON task.taskId = taskStatus.taskId;

CREATE OR REPLACE VIEW GroupTaskView AS
    SELECT
           taskStatus.userID as statusUserId, task.taskId, task.title, task.description,
           task.raplaLink, task.creationDate, task.dueDate,
           task.lastUpdated, task.createdBy, task.type,
           parent, status, GroupTask.groupId, UG.name as groupName
    FROM task
        JOIN GroupTask ON task.taskId = GroupTask.groupTaskId
        JOIN UnitasksGroup UG on GroupTask.groupId = UG.groupId
        JOIN taskStatus ON task.taskId = taskStatus.taskId;
