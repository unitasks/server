#!/usr/bin/python
# -*- coding: utf-8 -*-
import base64

from flask import render_template
import bcrypt
import psycopg2.errors

import database.database
import utils


#
# Session
#


def create_jwt_token(user_id, expires):
    random_string = utils.random_alphanum_string(64)
    database.database.execute(
        "INSERT INTO jwt_token (userId, token, timeout) VALUES (%s, %s, now() + interval '" + str(expires) +
        " seconds')", [user_id, random_string])
    return random_string


def check_jwt_token(token):
    res = database.database.query(
        "SELECT userId, timeout FROM jwt_token WHERE token = %s AND now() < timeout;",
        [token])
    if len(res) == 1:
        return res[0][0]
    return None


def remove_jwt_token(user_id, token):
    database.database.execute(
        "DELETE FROM jwt_token WHERE userId = %s AND token = %s;", [
            user_id, token])


def jwt_token_cleanup():
    database.database.execute(
        "DELETE FROM jwt_token WHERE NOT now() < timeout;", [])


#
# User
#


# TODO Replace messages with error codes
def create_user(email: str, username: str, password: str, displayname: str):
    if not check_account_exists(username, email):
        if len(password) <= 72:
            pw_hash = utils.generate_password_hash(password)
            try:
                database.database.execute("INSERT INTO member (email, username, password, displayName) VALUES (%s, %s, %s, %s)",
                                          [email, username, pw_hash, displayname])
                return True  # Successfully created
            except psycopg2.errors.UniqueVoilation:
                return None
        else:
            return None
    else:
        return None


def check_email(email: str, password: str):
    password = password.encode('utf-8')
    res = database.database.query(
        "SELECT password, userId from member WHERE email = %s", [email])
    hash = res[0][0]
    hash = base64.b64decode(hash)
    if bcrypt.checkpw(password, hash):
        return res[0][1]
    return None


def check_user(user: str, password: str):
    password = password.encode('utf-8')
    res = database.database.query(
        "SELECT password, userId from member WHERE username = %s", [user])
    hash = res[0][0]
    hash = base64.b64decode(hash)
    if bcrypt.checkpw(password, hash):
        return res[0][1]
    return None


def get_user_info(user):
    res = database.database.query(
        "SELECT userId, email, userName, displayName from member WHERE userId = %s",
        [user])
    return res[0]


def get_user_info_by_username(username):
    res = database.database.query(
        "SELECT userId, email, userName from member WHERE username = %s",
        [username])
    return res[0]


def check_account_exists(user: str, email: str):
    return check_user_exists(user) or check_email_exists(email)


def check_user_exists(user: str):
    res = database.database.query(
        "SELECT count(*) from member WHERE username= %s", [user])
    return res[0][0] == 1


def check_user_exists_by_id(user_id: int):
    if user_id is not None:
        res = database.database.query(
            "SELECT count(*) from member WHERE userId= %s", [user_id])
        return res[0][0] == 1
    else:
        return False


def check_email_exists(email: str):
    res = database.database.query(
        "SELECT count(*) from member WHERE email= %s", [email])
    return res[0][0] == 1


def send_verification_email(username: str, email: str, confirmation_url: str):
    token = utils.random_alphanum_string(64)
    confirmation_link = confirmation_url + "?token=" + token

    user_id = get_user_info_by_username(username)[0]

    database.database.execute(
        "INSERT INTO Token (token, userid) VALUES (%s, %s)", [
            token, user_id])

    utils.send_email(
        email,
        f"{username}, please verify your unitasks.net account",
        render_template("inline-email-verification.html", username=username, confirmation_link=confirmation_link)
    )



def login(name: str, password: str):
    if check_email_exists(name):
        return check_email(name, password)
    elif check_user_exists(name):
        return check_user(name, password)
    else:
        return None
