#!/usr/bin/python
# -*- coding: utf-8 -*-
import gc
from functools import wraps

import configargparse

import gc

import psycopg2

args = configargparse.get_argument_parser().parse_args()
pool = None


def db_conn_required(f):
    global pool
    @wraps(f)
    def decorated_function(*args, **kwargs):
        conn = connect(None)
        conn.autocommit = True
        try:
            return_value = f(conn, *args, **kwargs)
        finally:
            conn.close()
        return return_value

    return decorated_function


def connect(database):
    return psycopg2.connect(host="db", dbname=database,
                            user="postgres", password=args.postgres_password)
    # pool = ThreadedConnectionPool(1, 50, host="db", user="postgres", password=args.postgres_password, database=database)


@db_conn_required
def setup_architecture(conn):
    # Read architecture from SQL file
    sql_query = open("database/architecture.sql").read()

    cur = conn.cursor()
    cur.execute(sql_query)
    cur.close()
    gc.collect()


@db_conn_required
def execute(conn, sql_query: str, variables: list):
    cur = conn.cursor()
    cur.execute(sql_query, variables)
    cur.close()
    gc.collect()


@db_conn_required
def query(conn, sql_query: str, variables: list = None):
    if variables is None:
        variables = []

    cur = conn.cursor()
    cur.execute(sql_query, variables)
    results = cur.fetchall()
    cur.close()
    gc.collect()

    return results
