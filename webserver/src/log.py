import sys
import logging


def init_logger():
    print("Initializing logger!")
    unitasks_logger = logging.getLogger("unitasks")
    unitasks_logger.setLevel(logging.DEBUG)

    console_out = logging.StreamHandler(stream=sys.stdout)
    console_out.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        '%(asctime)s.%(msecs)2d | p%(process)s | %(pathname)s:%(lineno)d | %(levelname)s | %(message)s',
        "%Y-%m-%d %H:%M:%S")

    console_out.setFormatter(formatter)

    unitasks_logger.addHandler(console_out)
    unitasks_logger.debug("Logger initialized!")
