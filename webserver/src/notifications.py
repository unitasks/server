import hopper_api
import database.database
import json
import logging
import configargparse

logger = logging.getLogger("unitasks")

args = configargparse.get_argument_parser().parse_args()

#
# Hopper
#

if args.local_version:
    notification_api = hopper_api.HopperApi(hopper_api.HopperDev)
else:
    notification_api = hopper_api.HopperApi(hopper_api.HopperProd)

notification_app = None


def get_hopper_app():
    result = database.database.query("""SELECT content FROM HopperApp WHERE appId = 1""", [])
    global notification_app

    if notification_app is None:
        if len(result) == 0:
            notification_app = notification_api.create_app("Unitasks",
                                                           "https://unitasks.net",
                                                           "{}/static/media/img_logo_logo_full.d51c1ffd.svg".format(args.base_url),
                                                           "{}/manageHopperSubs".format(args.base_url),
                                                           "info@unitasks.net")
            database.database.execute("""INSERT INTO HopperApp (appId, content) VALUES (1, %s)""", (notification_app.serialize(),))
        else:
            notification_app = notification_api.deserialize_app(json.dumps(result[0][0]))
    return notification_app


def refresh_hopper_certs():
    global notification_app
    if notification_app is None:
        notification_app = get_hopper_app()

    notification_app = notification_app.generate_new_keys()
    database.database.execute("""UPDATE HopperApp SET content = %s WHERE appId = 1""", (notification_app.serialize(),))


def create_notifications_for_group(group_id, heading, content):
    logger.info("Creating hopper messages for group {}".format(group_id))
    if notification_app is not None:
        result = database.database.query("""SELECT Member.hopperSubId FROM Member INNER JOIN GroupMember ON Member.userId = GroupMember.userId WHERE GroupMember.groupId = %s AND Member.hopperSubId IS NOT NULL""", (group_id,))
        for entry in result:
            notification_api.post_notification(entry[0], hopper_api.Notification.default(heading, content))


def get_subscription_url(hopper_username):
    global notification_app
    if notification_app is not None:
        return notification_app.create_subscribe_request("{}/hopperCallback".format(args.base_url), hopper_username)
    else:
        return None
