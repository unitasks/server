#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import os
# DO NOT DELETE THIS, IT INITIALISES OUR CONFIG
import config

import configargparse
from flask import Flask


from flask import redirect
from flask_jwt_extended import JWTManager

import database.database
import database.users
import log
import notifications

from api.member import api_member
from api.task import api_task
from api.tasks import api_tasks
from api.user import api_user
from api.message import api_message
from api.file import api_file
from api.group import api_group
from api.hopper import api_hopper

# DO NOT DELETE THIS, IT INITIALISES OUR CONFIG
import config

#
# Init
#

# Create tables if they don't exist already
database.database.setup_architecture()

database.users.jwt_token_cleanup()

args = configargparse.get_argument_parser().parse_args()

app = Flask(__name__)
jwt = JWTManager(app)

app.config["env"] = os.environ.get("FLASK_ENV")
app.config["FLASK_HOST_STATIC_FILES"] = os.environ.get(
    "FLASK_HOST_STATIC_FILES")
app.config['SECRET_KEY'] = args.flask_secret_app_key
app.config['JWT_SECRET_KEY'] = args.jwt_secret_key
#
# Routes
#

app.register_blueprint(api_tasks, url_prefix="/api")
app.register_blueprint(api_task, url_prefix="/api")
app.register_blueprint(api_member, url_prefix="/api")
app.register_blueprint(api_user, url_prefix="/api")
app.register_blueprint(api_message, url_prefix="/api")
app.register_blueprint(api_file, url_prefix="/api")
app.register_blueprint(api_group, url_prefix="/api")
app.register_blueprint(api_hopper, url_prefix="/api")

#
# Logger
#

log.init_logger()
logger = logging.getLogger("unitasks")

#
# Hopper
#

hopper_app = notifications.get_hopper_app()


@app.route("/favicon.jpg")
@app.route("/favicon.ico")
@app.route("/favicon.png")
def favicon():
    return redirect("/static/img/logo_small_cropped.png")


#
# Profiler
#
PROFILER_ENABLED = os.environ.get("ENABLE_PROFILING")
PROFILER_OUTPUT = os.environ.get("PROFILER_OUT")
if PROFILER_ENABLED == "true":
    logger.info("Profiler enabled!!!")
    from werkzeug.middleware.profiler import ProfilerMiddleware

    app = ProfilerMiddleware(
        app, profile_dir=PROFILER_OUTPUT, stream=None
    )
else:
    logger.info("Profiler disabled!!!")

#
# Dev run
#
if __name__ == "__main__" and os.environ.get("FLASK_ENV") == "development":
    app.run("localhost", 8080, debug=True)
