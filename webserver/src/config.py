#!/usr/bin/python
# -*- coding: utf-8 -*-
import configargparse

args_parser = configargparse.get_argument_parser()

args_parser.add(
    "--postgres_password",
    help="The password of the postgres database",
    type=str,
    env_var="POSTGRES_PASSWORD",
    required=True
)

args_parser.add(
    "--flask_secret_app_key",
    help="The secret key for the flask app",
    type=str,
    env_var="FLASK_SECRET_APP_KEY",
    required=True
)

args_parser.add(
    "--cookie_timeout",
    help="The time in hours for which the 'remember me' cookie is valid",
    type=str,
    env_var="COOKIE_TIMEOUT",
    required=True
)

args_parser.add(
    "--email_verification",
    help="Set to true to enable email verification",
    type=bool,
    env_var="EMAIL_VERIFICATION",
    required=True
)

args_parser.add(
    "--sendgrid_api_key",
    help="The sendgrid api key you want to send emails from",
    type=str,
    env_var="SENDGRID_API_KEY",
    required=True
)

args_parser.add(
    "--jwt_secret_key",
    help="The JWT signing key",
    type=str,
    env_var="JWT_SECRET_KEY",
    required=True
)

args_parser.add(
    "--base_url",
    help="The App's base url",
    type=str,
    env_var="BASE_URL",
    required=True
)

args_parser.add(
    "--local_version",
    help="is the app running local?",
    type=bool,
    env_var="LOCAL_VERSION",
    default=True,
    required=False
)
