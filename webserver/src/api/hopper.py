from flask import Blueprint
import database.database
from utils import login_required, get_current_user_info
import notifications
from flask import jsonify

api_hopper = Blueprint('api_hopper', __name__)


@api_hopper.route("/hopper/setSubscription/<string:subscription_id>", methods=['POST'])
@login_required
def set_subscription_id(subscription_id):
    database.database.execute("""UPDATE Member SET hopperSubId = %s WHERE userId = %s""", (subscription_id, get_current_user_info()[0]))
    return "OK", 200


@api_hopper.route("/hopper/getUrl", methods=['GET'])
@login_required
def get_subscription_url():
    return jsonify({'url': notifications.get_subscription_url(get_current_user_info()[2])}), 200


@api_hopper.route("/hopper/isSubscribed", methods=['GET'])
@login_required
def has_user_subscribed():
    result = database.database.query("""SELECT hopperSubId FROM Member WHERE userId = %s""", (get_current_user_info()[0],))
    if result[0][0] is None:
        return jsonify({'subscribed': False}), 200
    else:
        return jsonify({'subscribed': True}), 200
