import configargparse
from flask import Blueprint
from flask import jsonify
from flask import request,redirect
from flask_jwt_extended import create_access_token, get_jwt_identity

import database.users
from database.users import remove_jwt_token
from utils import get_current_user_info
from utils import login_required
import datetime

api_user = Blueprint('api_user', __name__)
args = configargparse.get_argument_parser().parse_args()


@api_user.route("/register", methods=['POST'])
def create_user():
    data = request.json
    email = data['email']
    username = data['username']
    password = data['password']
    displayname = data['displayname']

    if email != "" and username != "" and password != "" and displayname != "":
        if database.users.create_user(
                email, username, password, displayname) is not None:
            # Send verification email if enabled
            if args.email_verification:
                database.users.send_verification_email(
                    username, email, "https://" + request.headers.get("Host", "unitasks.net") + "/api/confirm")
                return jsonify({'verification_status': 'mail_sent'}), 200
            else:
                return jsonify({'verification_status': 'done'}), 200
        else:
            return jsonify({'error': 'DB entry could not be created!'}), 400


@api_user.route("/auth", methods=['POST'])
def login():
    data = request.json
    user = data['user']
    password = data['password']

    user_id = database.users.login(user, password)

    if user_id is not None:
        access_token = create_access_token(identity=database.users.create_jwt_token(user_id, 60 * 60 * 24 * 14),
                                           expires_delta=datetime.timedelta(days=14))
        return jsonify({'access_token': access_token})
    else:
        return jsonify(
            {'error': 'user / password combination was incorrect!'}), 400


@api_user.route("/testUser", methods=['GET'])
@login_required
def test_user():
    return jsonify({"jwt_identity": get_jwt_identity(),
                    "user_info": get_current_user_info()}), 200


@api_user.route("/userInfo", methods=['GET'])
@login_required
def user_info():
    current_user_info = get_current_user_info()
    return jsonify(
        {"username": current_user_info[2], "email": current_user_info[1], "displayName": current_user_info[3]}), 200


@api_user.route("/logout", methods=['POST'])
@login_required
def logout():
    remove_jwt_token(get_current_user_info()[0], get_jwt_identity())
    return jsonify({"success": True}), 200


@api_user.route("/confirm", methods=['GET'])
def confirm():
    token = request.args.get("token")

    res = database.database.query(
        "SELECT userId, timeout FROM Token WHERE token = %s AND extract(epoch from now() at time zone 'utc' at time zone 'utc') < extract(epoch from timeout at time zone 'utc' at time zone 'utc')",
        [token]
    )

    if len(res) == 1:
        user_id = res[0][0]

        database.database.execute("DELETE FROM Token WHERE token = %s", [token])
        database.database.execute("UPDATE member SET emailConfirmed = TRUE WHERE userID = %s", [user_id])
        return redirect("https://staging.unitasks.net/emailConfirmed", code=302)
    else:
        return redirect("https://staging.unitasks.net/emailError", code=302)

