from flask import Blueprint
from flask import jsonify
from flask import request

from utils import login_required, get_current_user_info, random_alphanum_string
from database import database
from database.database import db_conn_required

from psycopg2.extras import RealDictCursor
import psycopg2

api_group = Blueprint('api_group', __name__)


def is_authorized(group_id):
    result = database.query("""
        SELECT 1 FROM GroupMember WHERE groupId = %s AND userId = %s AND isGroupAdmin = true;
        """, [group_id, get_current_user_info()[0]])

    return len(result) == 1


@api_group.route("/groups", methods=['GET'])
@login_required
@db_conn_required
def get_all_groups(conn):
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute("""
    SELECT UnitasksGroup.groupId as groupId, UnitasksGroup.name as name, UnitasksGroup.description as description, (
        SELECT COUNT(*) FROM GroupMember WHERE GroupMember.groupId = UnitasksGroup.groupId
    ) as memberCount, GroupMember.isGroupAdmin as admin
    FROM UnitasksGroup INNER JOIN GroupMember ON GroupMember.groupId = UnitasksGroup.groupId
    WHERE GroupMember.userId = %s ORDER BY groupId DESC;
    """,
                (get_current_user_info()[0],))
    result = cur.fetchall()

    cur.close()

    groups = [dict(r) for r in result]

    return jsonify(groups), 200


@api_group.route("/group", methods=['POST'])
@login_required
def create_group():

    data = request.json
    name = data['name']
    description = data['description']

    result = database.query("""
    INSERT INTO UnitasksGroup (name, description) VALUES
    (%s, %s) RETURNING groupId;""", [name, description])

    group_id = result[0][0]

    database.execute("""
    INSERT INTO GroupMember (groupId, userId, isGroupAdmin) VALUES
    (%s, %s, true);
    """, [group_id, get_current_user_info()[0]])

    link_token = random_alphanum_string(32)

    database.execute("""
    INSERT INTO GroupJoinLink (groupId, token) VALUES
    (%s, %s);
    """, [group_id, link_token])

    return jsonify({"groupId": group_id, "linkToken": link_token}), 200


@api_group.route("/group/<int:group_id>", methods=['GET'])
@login_required
@db_conn_required
def get_group(conn, group_id):
    cur = conn.cursor(cursor_factory=RealDictCursor)
    cur.execute("""
    SELECT UnitasksGroup.name as name, UnitasksGroup.description as description, GroupJoinLink.token as join_link, GroupMember.isGroupAdmin as admin
     FROM UnitasksGroup
    INNER JOIN GroupJoinLink ON GroupJoinLink.groupId = UnitasksGroup.groupId INNER JOIN GroupMember ON GroupMember.groupId = UnitasksGroup.groupId
    WHERE UnitasksGroup.groupId = %s AND GroupMember.userId = %s;
    """, [group_id, get_current_user_info()[0]])

    group_info = cur.fetchall()

    print(group_info)

    if len(group_info) == 0:
        return "Group not found / no permission", 400

    cur.execute("""
    SELECT Member.userId as userId, Member.username as username, Member.displayName as displayName, GroupMember.isGroupAdmin as is_admin
     FROM Member INNER JOIN GroupMember ON GroupMember.userId = Member.userId WHERE GroupMember.groupId = %s;
    """, (group_id,))

    group_members = cur.fetchall()

    cur.close()

    group_dict = dict(group_info[0])

    if not group_dict['admin']:
        del group_dict['join_link']

    group_dict['members'] = [dict(m) for m in group_members]

    return jsonify(group_dict), 200


@api_group.route("/group/<int:group_id>", methods=['PUT'])
@login_required
def update_group(group_id):
    name = request.json['name']
    description = request.json['description']

    if not is_authorized(group_id):
        return "Not found / Not authorized", 400

    database.execute("""
        UPDATE UnitasksGroup SET name = %s, description = %s WHERE groupId = %s;
    """, [name, description, group_id])

    return "OK", 200


@api_group.route(
    "/group/<int:group_id>/member/<string:username>", methods=['POST'])
@login_required
def add_member(group_id, username):

    if not is_authorized(group_id):
        return "Not found / Not authorized", 400

    # Get user id
    user = database.query("""
    SELECT userId FROM Member WHERE Member.username=%s;
    """, (username,))

    if len(user) == 0:
        return "Username {} not found!".format(username), 400

    user_id = user[0][0]

    try:
        database.execute("""
        INSERT INTO GroupMember (groupId, userId, isGroupAdmin) VALUES
        (%s, %s, false);
        """, [group_id, user_id])
    except psycopg2.Error:
        return "Adding user failed, was the user already a member of the group?", 400

    return "OK", 200


@api_group.route("/group/<int:group_id>/member/<int:user_id>",
                 methods=['DELETE'])
@login_required
def remove_member(group_id, user_id):
    if not is_authorized(group_id):
        return "Not authorized", 400

    user = database.query("""
        SELECT isGroupAdmin FROM GroupMember WHERE userId=%s AND groupId=%s;
        """, (user_id, group_id))

    if len(user) == 0:
        return "User wasn't member of group", 400

    count_admins = database.query("""
            SELECT COUNT(*) FROM GroupMember WHERE groupId=%s AND isGroupAdmin = true;
            """, (group_id,))

    if user[0][0] and count_admins[0][0] < 2:
        return "Elect another admin before deleting!", 400

    database.execute("""
    DELETE FROM GroupMember WHERE userId=%s AND groupId=%s;
    """, (user_id, group_id))

    return "OK", 200


@api_group.route("/group/<int:group_id>/leave", methods=['DELETE'])
@login_required
def leave_member(group_id):
    user = database.query("""
        SELECT isGroupAdmin FROM GroupMember WHERE userId=%s AND groupId=%s;
        """, (get_current_user_info()[0], group_id))

    if len(user) == 0:
        return "You weren't member of the group", 400

    count_admins = database.query("""
            SELECT COUNT(*) FROM GroupMember WHERE groupId=%s AND isGroupAdmin = true;
            """, (group_id,))

    if user[0][0] and count_admins[0][0] < 2:
        return "Elect another admin before leaving!", 400

    database.execute("""
    DELETE FROM GroupMember WHERE userId=%s AND groupId=%s;
    """, (get_current_user_info()[0], group_id))

    return "OK", 200


@api_group.route("/group/<int:group_id>/member/<int:user_id>", methods=['PUT'])
@login_required
def update_member(group_id, user_id):
    if not is_authorized(group_id):
        return "Not authorized", 400

    is_admin = request.json['isAdmin']

    user_is_admin = database.query("""
        SELECT isGroupAdmin FROM GroupMember WHERE userId=%s AND groupId=%s;
        """, (user_id, group_id))

    if len(user_is_admin) == 0:
        return "User wasn't member of group", 400

    count_admins = database.query("""
            SELECT COUNT(*) FROM GroupMember WHERE groupId=%s AND isGroupAdmin = true;
            """, (group_id,))

    if user_is_admin[0][0] and not is_admin and count_admins[0][0] < 2:
        return "Elect another admin before giving up admin!", 400

    database.execute("""
    UPDATE GroupMember SET isGroupAdmin = %s WHERE userId = %s AND groupId = %s;
    """, (is_admin, user_id, group_id))

    return "OK", 200


@api_group.route("/group/join/<string:token>", methods=['POST'])
@login_required
def join_group_by_link(token):
    group_id = database.query("""
    SELECT groupId FROM GroupJoinLink WHERE token = %s;
    """, (token,))

    if len(group_id) == 0:
        return "Token does not exist", 400
    try:
        database.execute("""
        INSERT INTO GroupMember (groupId, userId, isGroupAdmin) VALUES
        (%s, %s, false);
        """, [group_id[0][0], get_current_user_info()[0]])
    except psycopg2.Error:
        return "Adding user failed, was the user already a member of the group?", 400

    return "OK", 200


@api_group.route("/group/<int:group_id>", methods=['DELETE'])
@login_required
def delete_group(group_id):
    if not is_authorized(group_id):
        return "Not authorized! / Not found!", 400

    database.execute("""
    DELETE FROM UnitasksGroup WHERE groupId = %s;
    """, (group_id,))

    return "OK", 200
