#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import json

from flask import Blueprint
from flask import jsonify
from flask import request
from psycopg2.extras import RealDictCursor

import database.database
import database.users
from utils import login_required, get_current_user_info

from api.utils import is_authorised_for_task

import notifications
import logging

from api.task import get_task_title, get_group_id_for_task, send_notification_to_task_group

api_message = Blueprint('api_message', __name__)
logger = logging.getLogger("unitasks")


def get_children_messages(cur, user_id, task_id,
                          message_id=None, last_updated=None):
    query = """
    SELECT
            messageId AS message_id,
            parentTask AS thread_id,
            parentMessage AS parent_id,
            userId, userId = %s AS editable,
            (SELECT displayName FROM member WHERE userId = ExchangeThreadMessage.userId) as owner_name,
            postDate AS post_date,
            lastEditedDate AS last_edited, lastEditedDate IS NOT NULL AS edited,
            content
        FROM ExchangeThreadMessage
        WHERE parentTask = %s
    """

    parameters = []
    # Add parent message to query
    # This has to be done by string concatenation since %s is only supported
    # for variables (not operators)
    if message_id is None:
        parameters = [user_id, task_id]
        query += "AND parentMessage IS NULL\n"
    elif isinstance(message_id, list) or isinstance(message_id, tuple):
        parameters = [user_id, task_id, tuple(message_id)]
        query += "AND (parentMessage IN %s OR parentMessage IS NULL)\n"
    else:
        parameters = [user_id, task_id, message_id]
        query += "AND parentMessage = %s\n"

    if last_updated is not None:
        parameters.append(last_updated)
        query += "AND GREATEST(postdate, lastediteddate) >= %s\n"

    query += " ORDER BY postDate ASC;"

    cur.execute(query, tuple(parameters))
    return cur.fetchall()


#
# Create
#


@api_message.route("/task/<int:task_id>/message",
                   methods=['POST'], defaults={"parent_message_id": -1})
@api_message.route(
    "/task/<int:task_id>/message/<int:parent_message_id>", methods=["POST"])
@login_required
def post_message(task_id: int, parent_message_id: int):
    content = json.dumps(request.json)
    user_id = get_current_user_info()[0]

    if content == "{}" or content == "":
        return "Bad request", 400

    if not is_authorised_for_task(task_id, user_id):
        return "Unauthorized", 401

    result = database.database.query("""
    INSERT INTO ExchangeThreadMessage (
        parentTask,
        parentMessage,
        userId,
        postDate,
        lastEditedDate,
        content
    )
    VALUES (%s, %s, %s, %s, %s, %s)
    RETURNING messageId
    """, (task_id,
          None if parent_message_id == -1 else parent_message_id,
          user_id, datetime.datetime.now().isoformat(), None, content))

    send_notification_to_task_group(task_id, "Exchange Board Message created",
                               "{} created a new message in the exchange board for task '{}'.".format(
                                   get_current_user_info()[3], get_task_title(task_id)
                               ))

    return jsonify({"status": "ok", "message_id": result[0][0]}), 200


#
# Read
#


@api_message.route("/task/<int:task_id>/message",
                   methods=['GET'], defaults={"message_id": -1})
@api_message.route(
    "/task/<int:task_id>/message/<int:message_id>", methods=["GET"])
@login_required
@database.database.db_conn_required
def load_messages(conn, task_id: int, message_id: int):
    user_id = get_current_user_info()[0]

    if not is_authorised_for_task(task_id, user_id):
        return "Unauthorized", 401

    # Get root messages, which are the leaf
    # If message_id is None, then the global root messages are loaded
    # Otherwise the the message and its children
    cur = conn.cursor(cursor_factory=RealDictCursor)
    root_messages = get_children_messages(
        cur, user_id, task_id, None if message_id == -1 else message_id)

    for i, message in enumerate(root_messages):
        children_messages = get_children_messages(
            cur, user_id, task_id, message["message_id"])
        if len(children_messages) > 0:
            root_messages[i]["children"] = children_messages
            root_messages[i]["has_children"] = True
        else:
            root_messages[i]["children"] = []
            root_messages[i]["has_children"] = False

    cur.close()
    return jsonify({"last_update": datetime.datetime.now(
    ).isoformat(), "messages": root_messages}), 200


@api_message.route("/task/<int:task_id>/message/update", methods=['GET'])
@login_required
@database.database.db_conn_required
def update_messages(conn, task_id: int):
    messages = json.loads(
        (request.args.get(
            "messages",
            default="[]",
            type=str)))
    last_updated = datetime.datetime.fromisoformat(
        request.args.get("last_updated", default=None, type=str))
    user_id = get_current_user_info()[0]

    if last_updated is None:
        return "Bad request", 400

    if not is_authorised_for_task(task_id, user_id):
        return "Unauthorized", 401

    if not messages:
        messages = None

    cur = conn.cursor(cursor_factory=RealDictCursor)
    required_messages = get_children_messages(
        cur, user_id, task_id, message_id=messages, last_updated=last_updated)

    for i, message in enumerate(required_messages):
        children_messages = get_children_messages(
            cur, user_id, task_id, message["message_id"])
        if len(children_messages) > 0:
            required_messages[i]["children"] = children_messages
            required_messages[i]["has_children"] = True
        else:
            required_messages[i]["children"] = []
            required_messages[i]["has_children"] = False

    cur.close()
    return jsonify(required_messages), 200


#
# Update
#


@api_message.route(
    "/task/<int:task_id>/message/<int:message_id>", methods=['PUT'])
@login_required
def edit_message(task_id: int, message_id: int):
    content = json.dumps(request.json)
    user_id = get_current_user_info()[0]

    # Is authorised to access thread
    if not is_authorised_for_task(task_id, user_id):
        return "Unauthorized", 401

    result = database.database.query("""SELECT * FROM ExchangeThreadMessage
                                        WHERE parentTask = %s AND userId = %s AND messageId = %s""",
                                     (task_id, user_id, message_id))

    if len(result) == 0:
        return "Unauthorized", 401

    database.database.execute("""
    UPDATE ExchangeThreadMessage SET content = %s, lastEditedDate = %s WHERE parentTask = %s AND userId = %s AND messageId = %s
    """, (content, datetime.datetime.now().isoformat(), task_id, user_id, message_id))

    send_notification_to_task_group(task_id, "Exchange Board Message altered",
                               "{} changed a message in the exchange board for task '{}'.".format(
                                   get_current_user_info()[3], get_task_title(task_id)
                               ))

    return "Ok", 200
