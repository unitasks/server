#!/usr/bin/python
# -*- coding: utf-8 -*-

import configargparse
from flask import Blueprint
from flask import jsonify
from flask import request

import database.database
from utils import login_required, push_message, get_current_user_info
from psycopg2.extras import RealDictCursor

from api.utils import is_authorised_for_task

import notifications

args = configargparse.get_argument_parser().parse_args()

api_task = Blueprint('api_task', __name__)


def send_notification_to_task_group(task, heading, content):
    group_id = get_group_id_for_task(task)

    if group_id is not None:
        notifications.create_notifications_for_group(group_id, heading, content)


def get_group_id_for_task(task_id):
    result = database.database.query("""SELECT GroupTask.groupId FROM GroupTask WHERE groupTaskId = %s""", (task_id,))
    if len(result) > 0:
        return result[0][0]
    else:
        return None


def get_task_title(task_id):
    return database.database.query("""SELECT title FROM Task WHERE taskId = %s""", (task_id,))[0][0]


@api_task.route("/task", methods=['POST'])
@login_required
def create_task():
    data = request.json

    title = data['title']
    description = data['description']
    datetime = data['datetime']
    is_subtask = data['is_subtask']
    root_task = data['root']
    is_personal = data['is_personal']

    if is_subtask:
        result = database.database.query(
            """SELECT type FROM Task WHERE taskId = %s""", (root_task,))

        if len(result) < 1:
            return "Task not found!", 400

        if result[0][0] == "group" and is_personal:
            return "Cannot add personal task to group task!", 400
        elif result[0][0] == "personal" and not is_personal:
            return "Cannot add group task to personal task!", 400

    if is_personal:
        if datetime is not None:

            taskid = database.database.query(
                "INSERT INTO Task (title, description, raplaLink, creationDate, dueDate, lastUpdated, createdBy, type) VALUES (%s, %s, %s, now(), %s, now(), %s, 'personal') RETURNING taskId;",
                [title, description, '-', datetime, get_current_user_info()[0]])[0][0]

            root = None

            if is_subtask:
                root = root_task

            database.database.execute("INSERT INTO PersonalTask (personalTaskId, parent, userId) VALUES (%s, %s, %s);",
                                      [taskid, root, get_current_user_info()[0]])
            database.database.execute("INSERT INTO TaskStatus (taskId, userId, status) VALUES (%s, %s, 'pending');",
                                      [taskid, get_current_user_info()[0]])

            push_message(
                'Created task ' +
                str(taskid),
                'Is subtask: ' +
                str(is_subtask))

            return jsonify({
                'taskid': taskid
            })
    else:
        group_id = data['group_id']

        result = database.database.query(
            """SELECT 1 FROM GroupMember WHERE groupId = %s AND userId = %s AND isGroupAdmin = true""",
            [group_id, get_current_user_info()[0]])

        if len(result) < 1:
            return "Unauthorized", 401

        taskid = database.database.query(
            "INSERT INTO Task (title, description, raplaLink, creationDate, dueDate, lastUpdated, createdBy, type) VALUES (%s, %s, %s, now(), %s, now(), %s, 'group') RETURNING taskId;",
            [title, description, '-', datetime, get_current_user_info()[0]])[0][0]

        root = None

        if is_subtask:
            root = root_task

        database.database.execute("""INSERT INTO GroupTask (groupTaskId, parent, groupId) VALUES (%s, %s, %s)""",
                                  [taskid, root, group_id])

        database.database.execute(
            """INSERT INTO TaskStatus (taskId, userId, status) SELECT %s, GroupMember.userId, 'pending' FROM GroupMember WHERE groupId = %s;""",
            [taskid, group_id])

        send_notification_to_task_group(taskid, "Task Created!",
                                   "{} created the task '{}' !.".format(
                                       get_current_user_info()[3], title
                                   ))

        return jsonify({
            'taskid': taskid
        }), 200

    return "ERROR", 500


@api_task.route("/task/<int:taskid>", methods=['PUT'])
@login_required
def update_task(taskid):
    data = request.json

    title = data['title']
    description = data['description']
    datetime = data['datetime']

    result = database.database.query(
        """SELECT type FROM Task WHERE taskId = %s""", (taskid,))

    if len(result) < 1:
        return "Task not found", 400

    if is_authorised_for_task(
            taskid, get_current_user_info()[0], is_admin=True):
        database.database.execute(
            "UPDATE Task SET title = %s, description = %s, raplaLink = %s, dueDate = %s, lastUpdated = now() WHERE taskId = %s;",
            [title, description, '-', datetime, taskid])
    else:
        return "Unauthorized", 401

    send_notification_to_task_group(taskid, "Task Updated!",
                               "{} updated the task '{}' !.".format(
                                   get_current_user_info()[3], get_task_title(taskid)
                               ))
    push_message("Successfully updated task!", "The task has been updated in our system.")

    return "OK", 200


# I hope this is okay...
@api_task.route("/task/<int:task_id>", methods=['DELETE'])
@login_required
def delete_task(task_id):
    if is_authorised_for_task(
            task_id, get_current_user_info()[0], is_admin=True):
        database.database.execute(
            "DELETE FROM Task WHERE taskId = %s;", (task_id,))

        send_notification_to_task_group(task_id, "Task Deleted!",
                                        "{} deleted the task '{}' !.".format(
                                            get_current_user_info()[3], get_task_title(task_id)
                                        ))

        return "OK", 200
    else:
        return "Unauthorized", 401


@api_task.route("/task/set_state/<int:task_id>/<string:state>",
                methods=['PUT'])
@login_required
def set_task_state(task_id, state):
    if is_authorised_for_task(task_id, get_current_user_info()[0]):

        database.database.execute("UPDATE TaskStatus SET status = %s WHERE taskId = %s AND userId = %s",
                                  [state, task_id, get_current_user_info()[0]])

        return "OK", 200
    else:
        return "Unauthorized", 401


@api_task.route("/task/<int:task_id>", methods=['GET'])
@login_required
@database.database.db_conn_required
def get_task(conn, task_id):
    if is_authorised_for_task(task_id, get_current_user_info()[0]):
        task = database.database.query(
            """SELECT type FROM Task WHERE taskId = %s""", (task_id,))

        if task[0][0] == 'personal':
            cur = conn.cursor(cursor_factory=RealDictCursor)
            cur.execute("SELECT * FROM MemberTask WHERE userid = %s AND taskId = %s;",
                        [get_current_user_info()[0], task_id])
            result = cur.fetchall()

            cur.close()
            return jsonify(dict(result[0])), 200

        elif task[0][0] == 'group':
            cur = conn.cursor(cursor_factory=RealDictCursor)
            cur.execute("""SELECT GroupTaskView.*, GroupMember.isGroupAdmin as admin FROM GroupTaskView
                        INNER JOIN GroupMember ON GroupMember.groupId = GroupTaskView.groupId
                        WHERE taskId = %s AND GroupMember.userId = %s;""",
                        [task_id, get_current_user_info()[0]])
            result = cur.fetchall()

            cur.close()
            return jsonify(dict(result[0])), 200
    else:
        return "Unauthorized", 401
