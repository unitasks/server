#!/usr/bin/python
# -*- coding: utf-8 -*-


import configargparse

from flask import Blueprint
from flask import jsonify
from flask import request
from flask import session
from psycopg2.extras import RealDictCursor

import utils
from database.database import db_conn_required
from utils import get_current_user_info

args = configargparse.get_argument_parser().parse_args()

api_member = Blueprint('api_member', __name__)


@api_member.route("/member", methods=['GET'])
@utils.login_required
@db_conn_required
def get_member(conn):
    dict_cur = conn.cursor(cursor_factory=RealDictCursor)
    dict_cur.execute(
        """SELECT email, emailConfirmed, username, displayName
            FROM member
            WHERE userId = %s""",
        (get_current_user_info()[0],))
    res = dict_cur.fetchone()
    dict_cur.close()

    return jsonify(res), 200


@api_member.route("/member", methods=["PUT"])
@utils.login_required
@db_conn_required
def update_member(conn):
    if request.json is None:
        return "Bad Request", 400

    query_string = "UPDATE member SET"
    checked_data = {}

    # TODO: Check if email is in a valid format
    if "email" in request.json and len(request.json["email"]) <= 64:
        checked_data["email"] = request.json["email"]
        checked_data["emailconfirmed"] = False

    if "displayname" in request.json and len(
            request.json["displayname"]) <= 128:
        checked_data["displayname"] = request.json["displayname"]

    if "password" in request.json and len(request.json["password"]) <= 72:
        checked_data["password"] = utils.generate_password_hash(
            request.json["password"])

    number_of_keys = len(checked_data.keys())
    if number_of_keys == 0:
        return "Bad Request", 400

    parameters = []
    for i, key in enumerate(checked_data.keys()):
        parameters.append(checked_data[key])
        query_string += " " + key + " = %s"
        if i < number_of_keys - 1:
            query_string += ","

    query_string += " WHERE userid = %s"
    parameters.append(get_current_user_info()[0])

    cur = conn.cursor()
    cur.execute(query_string, parameters)
    cur.close()

    return "OK", 200


@api_member.route("/member", methods=['DELETE'])
@utils.login_required
@db_conn_required
def delete_member(conn):
    user_id = get_current_user_info()[0]

    cur = conn.cursor()
    cur.execute("DELETE FROM member where userid = %s", (user_id,))
    cur.close()

    session.clear()

    return "OK", 200
