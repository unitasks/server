from flask import Blueprint
from flask import request
from flask import jsonify
from flask import Response

from utils import login_required
from database import database
import io


api_file = Blueprint('api_file', __name__)


@api_file.route('/file', methods=['POST'])
@login_required
def upload_file():
    file = request.files['file']
    data = file.read()

    print(file.filename)
    print(data)

    file_id = database.query("""
        INSERT INTO file (fileName, fileData) VALUES (%s, %s) RETURNING fileId
    """, (file.filename, data))

    return jsonify({"fileId": file_id[0][0]}), 200


@api_file.route('/file/<int:file_id>', methods=['GET'])
@login_required
def download_file(file_id):
    file_info = database.query(
        """SELECT fileName, fileData FROM file WHERE fileId = %s""", (file_id,))

    data = io.BytesIO(bytes(file_info[0][1]))

    response = Response(data, mimetype='application/octet-stream')
    response.headers['filename'] = file_info[0][0]

    return response
