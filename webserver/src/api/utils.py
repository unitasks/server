import database.database


def is_authorised_for_task(task_id, user_id, is_admin=False):
    result = database.database.query(
        "SELECT type FROM task WHERE taskId = %s", (task_id,))

    if len(result) != 1:
        return False

    # Authorisation check
    if result[0][0] == "personal":
        result = database.database.query("SELECT * FROM PersonalTask WHERE personalTaskId = %s AND userId = %s",
                                         (task_id, user_id,))
        if len(result) != 1:
            return False

    elif result[0][0] == "group":
        if not is_admin:
            result = database.database.query("""
            SELECT * FROM GroupTask
                WHERE groupTaskId = %s AND
                      groupId IN (
                        SELECT DISTINCT GroupMember.groupId FROM GroupMember WHERE userId = %s
                      )
            """, (task_id, user_id))
        else:
            result = database.database.query("""
                        SELECT * FROM GroupTask
                            WHERE groupTaskId = %s AND
                                  groupId IN (
                                    SELECT DISTINCT GroupMember.groupId FROM GroupMember WHERE userId = %s AND isGroupAdmin = true
                                  )
                        """, (task_id, user_id))

        if len(result) != 1:
            return False

    return True
