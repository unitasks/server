#!/usr/bin/python
# -*- coding: utf-8 -*-
import configargparse
from flask import Blueprint
from flask import jsonify
from psycopg2.extras import RealDictCursor

from database.database import db_conn_required

from utils import login_required, get_current_user_info

import database.database

args = configargparse.get_argument_parser().parse_args()

api_tasks = Blueprint('api_tasks', __name__)


@api_tasks.route("/tasks", methods=['GET'])
@login_required
@db_conn_required
def get_tasks(conn):
    cur = conn.cursor(cursor_factory=RealDictCursor)

    user_info = get_current_user_info()

    cur.execute("SELECT * FROM MemberTask WHERE userid = %s ORDER BY dueDate ASC, title DESC;",
                (user_info[0],))
    result_member_tasks = cur.fetchall()

    member_tasks = [dict(r) for r in result_member_tasks]

    cur.execute("""SELECT DISTINCT GroupTaskView.*, GroupMember.isGroupAdmin as admin FROM GroupTaskView JOIN GroupMember ON GroupMember.groupId = GroupTaskView.groupId
                    WHERE GroupMember.userId = %s AND statusUserId = %s;""", (user_info[0], user_info[0]))

    result_group_tasks = cur.fetchall()

    cur.close()

    group_tasks = [dict(r) for r in result_group_tasks]

    root_tasks = []

    for t in member_tasks:
        if t['parent'] is None:
            t['subtasks'] = []
            root_tasks.append(t)

    for t in group_tasks:
        if t['parent'] is None:
            t['subtasks'] = []
            root_tasks.append(t)

    for t in member_tasks:
        if t['parent'] is not None:
            for r in root_tasks:
                if r['taskid'] == t['parent']:
                    r['subtasks'].append(t)

    for t in group_tasks:
        if t['parent'] is not None:
            for r in root_tasks:
                if r['taskid'] == t['parent']:
                    r['subtasks'].append(t)

    return jsonify(root_tasks), 200


@api_tasks.route("/subtasks/<int:rootid>", methods=['GET'])
@login_required
@db_conn_required
def get_subtasks(conn, rootid):
    result = database.database.query(
        """SELECT type FROM Task WHERE taskId = %s""", (rootid,))

    if len(result) < 1:
        return "Task not found", 400

    cur = conn.cursor(cursor_factory=RealDictCursor)

    user_info = get_current_user_info()

    if result[0][0] == 'personal':
        cur.execute("SELECT * FROM MemberTask WHERE userid = %s AND parent = %s ORDER BY dueDate ASC, title DESC;",
                    (user_info[0], rootid))
        result_member_tasks = cur.fetchall()
        cur.close()

        tasks = [dict(r) for r in result_member_tasks]

        return jsonify(tasks), 200
    elif result[0][0] == 'group':

        cur.execute("""SELECT GroupTaskView.*, GroupMember.isGroupAdmin as admin FROM GroupTaskView
                        INNER JOIN GroupMember ON GroupMember.groupId = GroupTaskView.groupId
                        WHERE GroupMember.userId = %s AND parent = %s AND statusUserId = %s ORDER BY dueDate ASC, title DESC;""",
                    (user_info[0], rootid, user_info[0]))
        result_group_tasks = cur.fetchall()
        cur.close()

        tasks = [dict(r) for r in result_group_tasks]

        return jsonify(tasks), 200
