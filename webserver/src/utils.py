#!/usr/bin/python
# -*- coding: utf-8 -*-
import random
import string
import traceback
import base64
from functools import wraps

import bcrypt

import logging

from flask import jsonify
import configargparse

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

import database.database
import database.users
from flask_jwt_extended import jwt_required, get_jwt_identity


args = configargparse.get_argument_parser().parse_args()

logger = logging.getLogger("unitasks")

#
# Static
#


def random_alphanum_string(length):
    return ''.join(random.choices(string.ascii_uppercase +
                                  string.digits + string.ascii_lowercase, k=length))

#
# login_required Wrapper
#


def login_required(f):
    @wraps(f)
    @jwt_required
    def decorated_function(*args, **kwargs):
        logger.debug("@login_required called!")
        try:
            if __check_login():
                logger.debug("user is logged in!")
                return f(*args, **kwargs)
            else:
                logger.debug("user is not logged in!")
                return jsonify({"error": "login check failed!"}), 400
        except Exception as err:
            logger.error("an error occured!")
            traceback.print_tb(err.__traceback__)
            print(err)
            return jsonify({"error": "an error occured at runtime!"}), 400
    decorated_function._original = f
    return decorated_function


def get_current_user_info():
    return database.users.get_user_info(
        database.users.check_jwt_token(get_jwt_identity()))


def __check_login():
    logger.debug("__check_login() called!")

    user_id = database.users.check_jwt_token(get_jwt_identity())

    db_logged_in = database.users.check_user_exists_by_id(user_id)
    logger.debug("Database login returned {}!".format(db_logged_in))

    if db_logged_in:
        logger.debug("Loggin check returnes True!")
        return True
    else:
        logger.debug("returning with false - login check was not successful")
    return False


def push_error_message(title, content):
    pass


def push_message(title, content):
    pass


def pop_error_messages():
    pass


def pop_messages():
    pass

#
# E-Mail
#


def send_email(to_emails, subject, message, from_email="noreply@unitasks.net"):
    message = Mail(
        from_email=from_email,
        to_emails=to_emails,
        subject=subject,
        html_content=message)
    try:
        sg = SendGridAPIClient(args.sendgrid_api_key)
        print("Sending verification email to '" + str(to_emails) + "'..")
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(str(e))
        print(e.body)

#
# Password
#


def generate_password_hash(password: str):
    password = password.encode('utf-8')
    password_hash = bcrypt.hashpw(password, bcrypt.gensalt())
    return base64.b64encode(password_hash).decode('utf-8')


class StringTemplate:
    def __init__(self, template):
        self.template = string.Template(template)
        self.partial_substituted_str = None

    def __repr__(self):
        return self.template.safe_substitute()

    def format(self, *args, **kws):
        self.partial_substituted_str = self.template.safe_substitute(
            *args, **kws)
        self.template = string.Template(self.partial_substituted_str)
        return self.__repr__()
