from behave import given, when, then
from features.steps import connector
from faker import Faker
import random
import string

myFaker = Faker()


def get_fields():
    username = connector.chrome_driver.find_element_by_id('username')
    email = connector.chrome_driver.find_element_by_id('email')
    password = connector.chrome_driver.find_element_by_id('pwd')
    repeat_password = connector.chrome_driver.find_element_by_id('repeat_pwd')

    return username, email, password, repeat_password


@given('user is on register page')
def is_on_register_page(context):
    if connector.is_connected():
        connector.chrome_driver.get("http://webserver/register")
        heading = connector.chrome_driver.find_element_by_tag_name("h1")
        assert heading.text == "Register"
    else:
        assert False


@given('user filled register form')
def fill_register_form(context):
    username, email, password, repeat_password = get_fields()

    my_password = ''.join(random.choices(string.ascii_uppercase + string.digits, k=12))

    username.send_keys(myFaker.first_name()+myFaker.last_name())
    email.send_keys(myFaker.email())
    password.send_keys(my_password)
    repeat_password.send_keys(my_password)

    assert True


@when('user presses confirm button')
def press_confirm(context):
    connector.chrome_driver.find_element_by_name("submit").click()
    assert True


@then('user receives confirmation mail')
def receive_confirmation_email(context):
    # WIP
    heading = connector.chrome_driver.find_element_by_tag_name("h1")
    assert heading.text == "Sign In"


@given('user filled register form password incorrect')
def fill_register_form_incorrect(context):
    username, email, password, repeat_password = get_fields()

    my_password = ''.join(random.choices(string.ascii_uppercase + string.digits, k=12))
    my_password_second = ''.join(random.choices(string.ascii_uppercase + string.digits, k=13))

    username.send_keys(myFaker.first_name() + myFaker.last_name())
    email.send_keys(myFaker.email())
    password.send_keys(my_password)
    repeat_password.send_keys(my_password_second)

    assert True


@then('user is prompted to correct password')
def check_password_prompt(context):
    alert = connector.chrome_driver.find_element_by_class_name("alert-danger").find_element_by_tag_name("strong")
    assert alert.text == "Passwords didn't match"


@given('user filled {field_name} field incorrect')
def fill_field_not(context, field_name):
    username, email, password, repeat_password = get_fields()

    if field_name == "username":
        email.send_keys(myFaker.email())
    elif field_name == "email":
        username.send_keys(myFaker.first_name() + myFaker.last_name())

    my_password = ''.join(random.choices(string.ascii_uppercase + string.digits, k=12))
    password.send_keys(my_password)
    repeat_password.send_keys(my_password)

    assert True


@then('user is prompted to correct field {field_name}')
def check_correct_field_prompt(context, field_name):
    alert = connector.chrome_driver.find_element_by_class_name("alert-danger").find_element_by_tag_name("strong")
    assert alert.text == "All fields must be filled!"
