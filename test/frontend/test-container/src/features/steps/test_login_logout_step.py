from behave import given, when, then
from features.steps import connector
from faker import Faker


def get_fields_register():
    username = connector.chrome_driver.find_element_by_id('username')
    email = connector.chrome_driver.find_element_by_id('email')
    password = connector.chrome_driver.find_element_by_id('pwd')
    repeat_password = connector.chrome_driver.find_element_by_id('repeat_pwd')

    return username, email, password, repeat_password


def get_fields():
    userfield = connector.chrome_driver.find_element_by_id('name')
    password = connector.chrome_driver.find_element_by_id('pwd')

    return userfield, password


@given('user is on landing page')
def is_on_login_page(context):
    if connector.is_connected():
        connector.chrome_driver.get("http://webserver/login")
        heading = connector.chrome_driver.find_element_by_tag_name("h1")
        assert heading.text == "Sign In"
    else:
        assert False


@given('user {username} {password} is registered')
def is_registered(context, username, password):
    myFaker = Faker()
    if connector.is_connected():
        connector.chrome_driver.get("http://webserver/register")
        username_, email, password_, repeat_password = get_fields_register()
        username_.send_keys(username)
        password_.send_keys(password)
        email.send_keys(myFaker.email())
        repeat_password.send_keys(password)

        connector.chrome_driver.find_element_by_name("submit").click()
        heading = ""
        try:
            heading = connector.chrome_driver.find_element_by_tag_name("h1")
        except Exception:
            pass
        alert = ""
        try:
            alert = connector.chrome_driver.find_element_by_class_name("alert-danger").find_element_by_tag_name("strong")
        except Exception:
            pass
        assert heading.text == "Sign In" or alert.text == "User already exists!"


@given('user {username} {password} filled login form valid')
def fill_login(context, username, password):
    user, password_ = get_fields()

    user.send_keys(username)
    password_.send_keys(password)

    assert True


@when('user presses login button')
def press_login_button(context):
    connector.chrome_driver.find_element_by_name("submit").click()
    assert True


@then('user is logged in')
def check_logged_in(context):
    try:
        connector.chrome_driver.find_element_by_id("logout")
        assert True
    except:
        print(connector.chrome_driver.page_source)
        assert False


@then('is redirected to their task list.')
def check_redirect_task(context):
    assert True


@given('user {username} {password} filled login form invalid')
def fill_login_invalid(context, username, password):
    user, password_ = get_fields()

    user.send_keys(username)
    password_.send_keys(password+"-")

    assert True


@then('user is prompted to correct his input.')
def check_correct_prompt(context):
    alert = connector.chrome_driver.find_element_by_class_name("alert-danger").find_element_by_tag_name("strong")
    assert alert.text == "Login failed!"


@when('user presses logout button')
def press_logout_button(context):
    connector.chrome_driver.find_element_by_id("logout").click()
    assert True


@then('user is logged out')
def check_user_logged_out(context):
    cookies = connector.chrome_driver.get_cookies()
    assert 'session' not in cookies


@given('user {username} {password} is logged in')
def login_user(context, username, password):
    connector.chrome_driver.delete_all_cookies()  # make shure u are logged out...
    is_on_login_page(context)
    fill_login(context, username, password)
    press_login_button(context)
    # logged in...

    print(connector.chrome_driver.page_source)

    check_logged_in(context)

