from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time

chrome_driver: webdriver.Remote = None
firefox_driver: webdriver.Remote = None


def connect():
    global chrome_driver, firefox_driver
    print("Waiting for Selenium Hub to start...")
    time.sleep(10)
    print("Connecting to Selenium Hub...")

    chrome_driver = webdriver.Remote(
        command_executor="http://selenium-hub:4444/wd/hub",
        desired_capabilities=DesiredCapabilities.CHROME
    )

    firefox_driver = webdriver.Remote(
        command_executor="http://selenium-hub:4444/wd/hub",
        desired_capabilities=DesiredCapabilities.FIREFOX
    )

    return chrome_driver, firefox_driver


def is_connected():
    if chrome_driver is None or firefox_driver is None:
        try:
            connect()
        except Exception:
            return False
    return True
