from behave import given, when, then
from features.steps import connector


@given("the server is online")
def wait_online(context):
    assert connector.is_connected()


@when("I connect to server")
def connect_server(context):
    connector.chrome_driver.get("http://webserver")
    assert True


@then("I am on the login page")
def check_login_page(context):
    heading = connector.chrome_driver.find_element_by_tag_name("h1")
    assert heading.text == "Sign In"

