Feature: Login and Logout
    As a registered user
    I want to login/logout myself
    so that I can sign in and sign out of the system.
    
Scenario Outline: valid login
    Given user <username> <password> is registered
    And user is on landing page
    And user <username> <password> filled login form valid
    When user presses login button
    Then user is logged in
    And is redirected to their task list.

    Examples:
    | username     | password |
    | HendrikLeier | passwort |

Scenario Outline: invalid login
    Given user is on landing page
    And user <username> <password> filled login form invalid
    When user presses login button
    Then user is prompted to correct his input.

    Examples:
    | username     | password |
    | HendrikLeier | passwort |
    
Scenario Outline: logout
    Given user <username> <password> is registered
    And user <username> <password> is logged in
    When user presses logout button
    Then user is logged out

    Examples:
    | username     | password |
    | HendrikLeier | passwort |