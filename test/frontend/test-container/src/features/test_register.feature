Feature: Register

   This feature allows the user to register himself.

Scenario: valid form fill
    Given user is on register page
    And user filled register form
    When user presses confirm button
    Then user receives confirmation mail

Scenario: invalid form fill password
    Given user is on register page
    And user filled register form password incorrect
    When user presses confirm button
    Then user is prompted to correct password

Scenario Outline: invalid form fill field
    Given user is on register page
    And user filled <field_name> field incorrect
    When user presses confirm button
    Then user is prompted to correct field <field_name>

    Examples:
    | field_name |
    | username   |
    | email      |


# --- WIP ---
# Scenario: confirm Mail
#     Given user has received confirmation mail
#     When user clicks on confirmation link
#     Then system creates a new account
#     And user is redirected to logged in landing page
