from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


def run():
    print("Waiting for Selenium Hub to start...")
    print("Connecting to Selenium Hub...")

    chrome_driver = webdriver.Remote(
        command_executor="http://selenium-hub:4444/wd/hub",
        desired_capabilities=DesiredCapabilities.CHROME
    )

    firefox_driver = webdriver.Remote(
        command_executor="http://selenium-hub:4444/wd/hub",
        desired_capabilities=DesiredCapabilities.FIREFOX
    )

    chrome_driver.get("http://webserver")

    print(chrome_driver.page_source)


if __name__ == "__main__":
    run()
