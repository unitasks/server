import unittest
import sys
from unittest import mock
from unittest.mock import patch, call
import utils
from utils import __check_login as check_login


class LoginTestUtils(unittest.TestCase):

    @patch("utils.get_jwt_identity")
    @patch("database.users.check_user_exists_by_id")
    @patch("database.users.check_jwt_token")
    def test__check_login(self, jwt, check_u_e, jwt_id):
        jwt.return_value = 1
        jwt_id.return_value = 1
        check_u_e.return_value = True
        result = check_login()

        jwt.assert_called_once_with(jwt_id.return_value)
        check_u_e.assert_called_once_with(jwt.return_value)
        jwt_id.assert_called_once()
        assert result


if __name__ == '__main__':
    unittest.main()
