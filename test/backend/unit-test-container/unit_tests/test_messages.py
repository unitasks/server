import unittest
from unittest import mock
from unittest.mock import patch
import sys
sys.modules['notifications'] = mock.MagicMock()  # Disable notifications class...
from api.message import get_children_messages
from api.message import post_message


class TestMessages(unittest.TestCase):

    def test_getChildren_messages(self):
        cursor_mock = mock.MagicMock()

        cursor_mock.fetchall.return_value = [[1, 2, 3], [4, 5, 6]]

        result = get_children_messages(cursor_mock, 1, 1, 0, 0)

        cursor_mock.execute.assert_called_once()

        assert result == cursor_mock.fetchall.return_value

    @patch("api.message.jsonify")
    @patch("api.message.get_current_user_info")
    @patch("database.database.query")
    @patch("api.message.is_authorised_for_task")
    @patch("api.message.request")
    def test_post_message(self, request, is_authorized, query, user_info, jsonify):
        request.json = {
            "test": "msg"
        }

        user_info.return_value = [1, "abc", "abc@gmail.com", "ABC DEF"]

        post_message._original(1, 32)

        query.assert_called()
        is_authorized.assert_called_once_with(1, 1)
        jsonify.assert_called_once()

