import unittest
import sys
from unittest import mock
from unittest.mock import patch, call
import base64
sys.modules['configargparse'] = mock.Mock()
import utils
import database.users
import database.database


# Mocks

class LoginTestCase(unittest.TestCase):
    @patch("bcrypt.checkpw")
    @patch("database.database.query")
    def test_check_user(self, query, checkpw):
        query_returns = [[utils.generate_password_hash("12234"), 12345]]
        query.return_value = query_returns
        checkpw.return_value = True
        result = database.users.check_user("abc", "12234")
        query.assert_called_once_with("SELECT password, userId from member WHERE username = %s", ["abc"])
        checkpw.assert_called_once_with(b'12234', base64.b64decode(query_returns[0][0]))
        assert result == 12345

    @patch("bcrypt.checkpw")
    @patch("database.database.query")
    def test_check_email(self, query, checkpw):
        query_returns = [[utils.generate_password_hash("12234"), 12345]]
        query.return_value = query_returns
        checkpw.return_value = True
        result = database.users.check_email("abc@gmail.com", "12234")
        query.assert_called_once_with("SELECT password, userId from member WHERE email = %s", ["abc@gmail.com"])
        checkpw.assert_called_once_with(b'12234', base64.b64decode(query_returns[0][0]))
        assert result == 12345

    @patch("database.database.query")
    def test_get_user_info(self, query):
        query_result = [[12345, "abc@gmail.com", "ABCD", "ABC DEFG"]]
        query.return_value = query_result
        result = database.users.get_user_info(12345)
        query.assert_called_once_with("SELECT userId, email, userName, displayName from member WHERE userId = %s", [12345])
        assert result == [12345, "abc@gmail.com", "ABCD", "ABC DEFG"]

    @patch("database.database.query")
    def test_check_account_exists(self, query):
        query_result = [[1]]
        query.return_value = query_result
        result = database.users.check_account_exists("abcde", "abc@gmail.com")
        # Lazy eval...
        query.assert_called_once_with("SELECT count(*) from member WHERE username= %s", ["abcde"])
        assert result


if __name__ == '__main__':
    unittest.main()
