import unittest
from unittest import mock
from unittest.mock import patch, call
import sys
sys.modules['notifications'] = mock.MagicMock()  # Disable notifications class...
import api.task


class TaskTestMethods(unittest.TestCase):
    @patch("api.task.jsonify")
    @patch("database.database.execute")
    @patch("api.task.get_current_user_info")
    @patch("database.database.query")
    @patch("api.task.notifications")
    @patch("api.task.request")
    def test_create(self, request, notifications, query, user_info, execute, jsonify):
        request.json = {
            "title": "test",
            "description": "This is a test",
            "datetime": "12/11/2012, 7:00:00 PM",
            "is_subtask": True,
            "root": 123,
            "is_personal": False,
            "group_id": 1
        }
        query.side_effect = [
            [["group"]],
            [[1]],
            [[124]],
            [[1]]
        ]

        user_info.return_value = [1, "abcd", "abc@gmail.com", "ABC DEF"]

        result = api.task.create_task._original()

        query.assert_called()
        execute.assert_called()
        user_info.assert_called()
        jsonify.assert_called()

        print(result)

    @patch("api.task.jsonify")
    @patch("database.database.execute")
    @patch("api.task.get_current_user_info")
    @patch("database.database.query")
    @patch("api.task.notifications")
    @patch("api.task.request")
    def test_create2(self, request, notifications, query, user_info, execute, jsonify):
        request.json = {
            "title": "test",
            "description": "This is a test",
            "datetime": "12/11/2012, 7:00:00 PM",
            "is_subtask": True,
            "root": 123,
            "is_personal": True,
        }
        query.side_effect = [
            [["personal"]],
            [[1]],
            [[124]],
            [[1]]
        ]

        user_info.return_value = [1, "abcd", "abc@gmail.com", "ABC DEF"]

        result = api.task.create_task._original()

        query.assert_called()
        execute.assert_called()
        user_info.assert_called()
        jsonify.assert_called()

        print(result)


if __name__ == '__main__':
    unittest.main()