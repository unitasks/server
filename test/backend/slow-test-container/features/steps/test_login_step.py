#!/usr/bin/python
# -*- coding: utf-8 -*-
from behave import given, when, then


@given("there are users")
def check_user(context):
    pass


@when("we login")
def login(context):
    assert True


@then("we are logged in")
def check_login(context):
    assert True
