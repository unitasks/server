#!/bin/bash

cat test/backend/unit-test-container/requirements.txt > test/backend/unit-test-container/temp_requirements.txt
cat webserver/requirements.txt >> test/backend/unit-test-container/temp_requirements.txt
sudo docker-compose -f test/backend/docker-compose.unit-test.yml build
sudo docker-compose -f test/backend/docker-compose.unit-test.yml up --abort-on-container-exit --exit-code-from unit_test
