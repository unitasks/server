#!/bin/bash

while getopts ":r:" opt; do
  case $opt in
    r)
      echo "-r option does not require any parameters" >&2
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Resetting database.." >&2
      sudo rm -r volumes/db
      mkdir volumes/db
      echo "Destroying old containers.." >&2
      sudo docker-compose -f docker-compose.dev.yml down
      ;;
  esac
done

sudo docker-compose -f docker-compose.dev.yml build
sudo docker-compose -f docker-compose.dev.yml up