#!/bin/bash

# POSTGRES_PASSWORD
echo -n "POSTGRES_PASSWORD=" > .env
echo "$1" >>.env

# FLASK_SECRET_APP_KEY
echo -n "FLASK_SECRET_APP_KEY=" >> .env
echo "$2" >>.env

# PWD
echo -n "PWD=" >> .env
pwd >> .env

# WEBPORT
echo -n "WEBPORT=" >> .env
echo "$3" >>.env

# COOKIE_TIMEOUT
echo -n "COOKIE_TIMEOUT=" >> .env
echo "$4" >>.env

# LOGGING_ADDRESS
echo -n "LOGGING_ADDRESS=" >> .env
echo "$5" >>.env

# LOGGING_TAG
echo -n "LOGGING_TAG=" >> .env
echo "$6" >>.env

# EMAIL_VERIFICATION
echo -n "EMAIL_VERIFICATION=" >> .env
echo "$7" >>.env

# SENDGRID_API_KEY
echo -n "SENDGRID_API_KEY=" >> .env
echo "$8" >>.env

# DOCKER_CONTAINER_WEBSERVER_URL
echo -n "DOCKER_CONTAINER_WEBSERVER_URL=" >> .env
echo "$9" >>.env

# DOCKER_CONTAINER_FRONTEND_URL
echo -n "DOCKER_CONTAINER_FRONTEND_URL=" >> .env
echo "${10}" >>.env

# JWT_SECRET_KEY
echo -n "JWT_SECRET_KEY=" >> .env
echo "${11}" >>.env

# BASE URL
echo -n "BASE_URL=" >> .env
echo "${12}" >>.env

# LOCAL VERSION
echo -n "LOCAL_VERSION=" >> .env
echo "${13}" >>.env
