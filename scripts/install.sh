#!/bin/bash

# For use on x86-64/amd64 Ubuntu machines

# Update & Upgrade
apt-get update && apt-get upgrade --no-install-recommends --no-install-suggests -y

# Install letsencrypt
apt-get install npm letsencrypt -y

# Install Requirements
npm install -g less node-minify

#
# Install Docker CE
#

sudo apt-get remove docker docker-engine docker.io containerd runc -y
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo docker run hello-world

#
# Install docker compose
#

sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose