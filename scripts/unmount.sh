#!/bin/bash

echo "Unmounting..."

while [ $? -eq 0 ]; do
    sudo umount ./test-backend/tmp_mount
done

echo "Unmounting done."